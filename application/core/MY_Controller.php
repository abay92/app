<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set("Asia/Jakarta");

/* load the MX_Router class */
require APPPATH . "third_party/MX/Controller.php";

class MY_Controller extends MX_Controller {
 
    function __construct() {
        parent::__construct();
        if (version_compare(CI_VERSION, '3.1.6', '<')) {
            $this->load->library('security');
        }
        $this->checkSession();
    }

    function checkSession(){
        if(!$this->session->userdata('app_session')){
            redirect(site_url());
        }else{
            $this->_sess = $this->session->userdata('app_session');
            if(date('Y-m-d H:i:s') > date($this->_sess['expired_login'])){
                $this->logoutSessionExp();
            }

            if(!is_login()){
                $this->session->unset_userdata('app_session');
                redirect(site_url());
            }
        }
    }

    function logoutSessionExp(){
        $session = $this->session->userdata('app_session');
        if($session){
            $dataUpdate = array(
                'user_id'   => decode($session['user']), 
                'is_login'  => 0,
            );
            $this->mglobal->updateData('t_mtr_user', $dataUpdate, 'user_id');
        }

        $this->session->unset_userdata('app_session');
        redirect(site_url());
    }
}