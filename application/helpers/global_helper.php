<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('json_api')){
    function json_api($status,$message,$data=''){
    	if($data == ''){
    		$array = array(
             'status'  => $status, 
             'message' => $message
         );
    	}else{
    		$array = array(
             'status'  => $status, 
             'message' => $message,
             'data'	  => $data
         );
    	}
    	
    	return json_encode($array);
    }
}

if(!function_exists('encode')){
    function encode($data){
        return strtr(rtrim(base64_encode($data), '='), '+/', '-_');
    }
}

if(!function_exists('decode')){
    function decode($base64){
        return base64_decode(strtr($base64, '-_', '+/'));
    }
}


if (!function_exists('date_indo')) {
    function date_indo($date) {
        $data = explode('-', $date);

        $month = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        );

        return "{$data[2]} {$month[$data[1]]} {$data[0]}";
    }

}

if (!function_exists('month_indo')) {
    function month_indo($month) {
        $arr = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        );

        return "{$arr[$month]}";
    }

}

if(!function_exists('is_login')){
    function is_login(){
        $CI   =& get_instance();
        $sess = $CI->session->userdata('app_session');
        $data = $CI->mglobal->selectDataByID('t_mtr_user', 'user_id', decode($sess['user']));

        if($sess['is_login'] == $data->is_login){
            return true;
        }else{
            return false;
        }
    }
}

if(!function_exists('validate_ajax')){
    function validate_ajax() {
        $CI   =& get_instance();
        if (!$CI->input->is_ajax_request()) {
          show_404();
      }
  }
}

if(!function_exists('listMenu')){
    function listMenu($data, $parent = 0) {
        if (isset($data[$parent])) {
            // $checkParent = listMenu($data, 0);
            if($parent == 0){
                $ul = '<ul class="sidebar-menu" data-widget="tree">';
                $ul .= '<li class="header">MAIN NAVIGATION</li>';
            }else{
                // if($checkParent){
                    $ul = '<ul class="treeview-menu">';
                // }
            }

            $html = "{$ul}";
            foreach ($data[$parent] as $v) {
                $child  = listMenu($data, $v->id);
                if($v->url == null || $v->url == '' || $v->url == '#'){
                    $link = '#';
                }else{
                    $link = site_url().$v->url;
                }

                $rpc = str_replace('/', '_', $v->url);

                if($child){
                    $html .= '<li class="treeview">';
                    $html .= '<a href="'.$link.'" id="'.$rpc.'"><i class="fa fa-'.$v->icon.'"></i>';
                    $html .= '<span> '.$v->name.'</span>';
                    $html .= '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';
                    $html .= '</a>';
                }else{
                    $html .= '<li><a href="'.$link.'" id="'.$rpc.'"><i class="fa fa-'.$v->icon.'"></i><span>'.$v->name.'</span></a>';
                }

                if($child){
                    $html .= $child;
                }
                $html .= '</li>';
            }

            $html .= "</ul>";
            return $html;
        }else{
            return false;
        }
    }
}

if(!function_exists('checkAccess')){
    function checkAccess($current_url,$action){
        $CI     =& get_instance();
        $session= $CI->session->userdata('app_session');
        $menu   = $session['menu'];
        $button = array();

        foreach ($menu as $key => $val) {
            $data = array_filter($menu[$key], function ($item) {
                if ($item->url == '#' || $item->url == '') return false;
                return true; 
            });

            foreach ($data as $k => $v) {
                $button[$v->url] = $v->action;
            }
        }

        if(isset($button[''.$current_url.''])){
            if(in_array($action, $button[''.$current_url.''], true)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}

if(!function_exists('checkUrlAccess')){
    function checkUrlAccess($current_url){
        $CI     =& get_instance();
        $session= $CI->session->userdata('app_session');
        $menu   = $session['menu'];
        $link   = array(
            'home'    => 'home',
            'profile' => 'profile'
        );

        foreach ($menu as $key => $val) {
            $data = array_filter($menu[$key], function ($item) {
                if ($item->url == '#' || $item->url == '') return false;
                return true; 
            });

            foreach ($data as $k => $v) {
                $link[$v->url] = $v->url;
            }
        }

        if(isset($link[''.$current_url.''])){
            return true;
        }else{
            return false;
        }
    }
}

if(!function_exists('createBtnAction')){
    function createBtnAction($slug,$action,$param=''){
        $button= '';
        $access= checkAccess($slug,$action);
        $url   = site_url($slug.'/'.$action);

        if($param != ''){
            $url   = site_url($slug.'/'.$action.'/'.$param);
        }

        if($access){
            if(strtolower($action) == 'add'){
                $button = '<button onclick="showModal(\''.$url.'\')" class="btn btn-primary btn-icon btn-sm" title="Tamabah"><i class="fa fa-plus"></i> Tambah</button> ';
            }

            else if(strtolower($action) == 'edit'){
                $button = '<button onclick="showModal(\''.$url.'\')" class="btn btn-primary btn-icon btn-xs" title="Edit"><i class="fa fa-pencil"></i></button> ';
            }

            elseif (strtolower($action) == 'delete') {
                $button = '<button onclick="confirmationAction(\'Apakah Anda Yakin Akan Menghapus Data ini?\',\''.$url.'\')" class="btn btn-danger btn-icon btn-xs" title="Hapus"><i class="fa fa-trash"></i></button> ';
            }

            elseif (strtolower($action) == 'change_password') {
                $button = '<button onclick="showModal(\''.$url.'\')" class="change-password btn btn-warning btn-icon btn-xs" title="Ganti Password"><i class="fa fa-lock"></i></button> ';
            }

            elseif (strtolower($action) == 'upload') {
                $button = '<button onclick="showModal(\''.$url.'\')" class="btn btn-primary btn-icon btn-sm" title="Upload"><i class="fa fa-upload"></i> Upload</button> ';
            }

            elseif (strtolower($action) == 'download') {
                $button = '<button id="download" class="btn btn-success btn-icon btn-sm" title="Download" disabled data-loading-text="<i class=\'fa fa-spinner fa-spin\'></i> Proses..."><i class="fa fa-download"></i> Download</button> ';
            }
        }

        return $button;
    }
}

if(!function_exists('headerForm')){
    function headerForm($title){
        $html  = '<div class="box-header with-border">';
        $html .= '<h1 class="box-title">'.$title.'</h1>';
        $html .= '<div class="box-tools pull-right">';
        $html .= '<button type="button" class="btn btn-box-tool" onclick="closeModal()"><i class="fa fa-times"></i></button></div></div>';

        return $html;
    }
}

if(!function_exists('createBtnForm')){
    function createBtnForm($type){
        $html  = '<div class="box-footer text-right">';
        $html .= '<button type="button" class="btn btn-default" onclick="closeModal()"><i class="fa fa-close"></i> Batal</button> ';
        $html .= '<button type="submit" class="btn btn-primary" id="saveBtn"><i class="fa fa-check"></i> '.$type.'</button>';
        $html .= '</div>';

        return $html;
    }
}

if(!function_exists('ipClient')){
    function ipClient() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}

if(!function_exists('dropdown_user_group')){
    function dropdown_user_group(){
        $CI         =& get_instance();
        $datas      = $CI->mglobal->selectAll('t_mtr_user_group');
        $session    = $CI->session->userdata('app_session');
        $data['']   = '';

        if($datas){
            foreach($datas as $row){
                $data[encode($row->group_id)] = $row->group_name;
            }
        }

        if($session['level'] != 1){
            unset($data[decode(1)]);
        }
        
        return $data;
    }
}

if(!function_exists('dropdown_company')){
    function dropdown_company(){
        $CI         =& get_instance();
        $datas      = $CI->mglobal->selectAll('t_mtr_company');
        $data['']   = '';

        if($datas){
            foreach($datas as $row){
                $data[encode($row->code)] = $row->name;
            }
        }
        
        return $data;
    }
}