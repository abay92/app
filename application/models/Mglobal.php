<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mglobal extends CI_Model {

	function __construct() {
        parent::__construct();
        $this->_sess = $this->session->userdata('app_session');
    }

	function checkLogin($where,$field) {
		$this->db->where($where, $field);
		return $this->db->get("t_mtr_user")->row();
	}

	function getMenu($group_id){
		$data = array();

        $sql = "SELECT 
            cc.menu_id as id,
            cc.name,
            cc.parent_id,
            cc.icon,
            cc.url,
            cc.ordering
        FROM
            t_mtr_privilege aa
        JOIN t_mtr_user_group bb ON aa.user_group_id = bb.group_id AND bb.active = 1 AND bb.group_id = $group_id
        JOIN t_mtr_menu cc ON aa.menu_id = cc.menu_id AND cc.active = 1
        JOIN t_mtr_menu_detail dd ON aa.menu_detail_id = dd.detail_id AND dd.active = 1
        JOIN t_mtr_action ee ON ee.action_id = dd.action_id AND ee.active = 1 AND UPPER(ee.action_name) = UPPER('view')
        WHERE aa.active = 1 ORDER BY cc.ordering ASC";

        $query = $this->db->query($sql)->result();

        foreach ($query as $row) {
        	$row->action = $this->checkAction($group_id,$row->id);
            $data[$row->parent_id][] = $row;
        }

        return $data;
 	}

	function countData($table) {
		$this->db->where('active', 1);
		return $this->db->get($table)->num_rows();
	}

	function selectAll($table) {
		$this->db->where('active', 1);
		return $this->db->get($table)->result();
	}

 	function checkAction($group_id,$menu_id){
		$data = array();

        $sql = "SELECT ee.action_name FROM t_mtr_privilege aa
	        JOIN t_mtr_user_group bb ON aa.user_group_id = bb.group_id AND bb.active = 1 AND bb.group_id = $group_id
	        JOIN t_mtr_menu cc ON aa.menu_id = cc.menu_id AND cc.active = 1 AND cc.menu_id = $menu_id
	        JOIN t_mtr_menu_detail dd ON aa.menu_detail_id = dd.detail_id AND dd.active = 1
	        JOIN t_mtr_action ee ON ee.action_id = dd.action_id AND ee.active = 1
	        WHERE aa.active = 1 ORDER BY cc.ordering ASC";

        $query = $this->db->query($sql)->result();

        foreach ($query as $row) {
            $data[] = $row->action_name;
        }

        return $data;
 	}

	function selectData($table, $where){
        $this->db->where('active', 1);
        return $this->db->get_where($table, $where)->result();
	}

	function selectDataByID($table, $where, $id){
		$this->db->where($where, $id);
		$this->db->where('active', 1);
		return $this->db->get($table)->row();
	}

	function selectWhereIn($table ,$where, $val){
		$this->db->where('active', 1);
		$this->db->where_in($where, $val);
		return $this->db->get($table)->result();
	}

	function checkData($table, $where, $fieldId='', $id=''){
        $this->db->where('active', 1);

        //check update
        if($fieldId != '' && $id != ''){
            $this->db->where("{$fieldId} != {$id}");
        }

        $this->db->limit(1);
        return $this->db->get_where($table, $where)->result();
    }

	function saveData($table, $data){
		if ($this->input->is_cli_request()){
			$user = 0;
		}else{
			$user = decode($this->_sess['user']);
		}
        
        $data['created_by'] = $user;
		$data['created_on'] = date('Y-m-d H:i:s');
		$this->db->insert($table, $data);
		
		if($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        }else{
            return $this->db->insert_id();
        }
	}

	function updateData($table, $data, $key){
		if ($this->input->is_cli_request()){
			$user = 0;
		}else{
			$user = decode($this->_sess['user']);
		}

		$data['updated_by'] = $user;
		$data['updated_on'] = date('Y-m-d H:i:s');
		$this->db->where($key,$data[$key]);
		unset($data[$key]);
		$this->db->update($table,$data);
		if($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        }else{
            return true;
        }
	}

	function update_data_by_not_id($table, $data, $key, $field){
		if ($this->input->is_cli_request()){
			$user = 0;
		}else{
			$user = decode($this->_sess['user']);
		}

		$data['updated_by'] = $user;
		$data['updated_on'] = date('Y-m-d H:i:s');
		$this->db->where('active', 1);
		$this->db->where('UPPER('.$key.')',strtoupper($field));
		$this->db->update($table,$data);
		if($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        }else{
            return true;
        }
	}

	function insertBatch($table, $arr){
		if ($this->input->is_cli_request()){
			$user = 0;
		}else{
			$user = decode($this->_sess['user']);
		}

		$data = array();
		foreach ($arr as $key => $value) {
            $value['created_by'] = $user;
            $value['created_on'] = date('Y-m-d H:i:s');

            $data[] = $value;
        }

		$this->db->insert_batch($table, $data);
		if($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        }else{
            return true;
        }
	}

	function updateBatch($table, $arr, $id){
		if ($this->input->is_cli_request()){
			$user = 0;
		}else{
			$user = decode($this->_sess['user']);
		}

		$data = array();
		foreach ($arr as $key => $value) {
			$value['updated_by'] = $user;
			$value['updated_on'] = date('Y-m-d H:i:s');

            $data[] = $value;
        }

		$this->db->update_batch($table, $data, $id);
		if($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        }else{
            return true;
        }
	} 

	function deleteDataById($table,$where,$field){
		$this->db->where($where, $field);
		$this->db->delete($table);
	    return $this->db->affected_rows();
	}

	function deleteDataMultiple($table,$where,$field){
		$this->db->where_in($where, $field);
		$this->db->delete($table);
	    return $this->db->affected_rows();
	}

	function saveLog($data){
		if ($this->input->is_cli_request()){
			$user = 0;
		}else{
			$user = decode($this->_sess['user']);
		}
        
        $data['created_by'] = $user;
		$data['created_on'] = date('Y-m-d H:i:s');
		$this->db->insert('t_log', $data);
		
		return $this->db->insert_id();
	}
}
