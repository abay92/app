<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * -------------------
 * CLASS NAME : Action
 * -------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Action extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('action_model');
        $this->module = 'action';
        $this->_table = 't_mtr_action';
    }
 	
 	function index() {
 		$data['title']   = "Aksi";
 		$data['content'] = "action/index";
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');

        $this->load->view('template/page', $data);
    }

    function add(){
        validate_ajax();
        $data['title'] = 'Tambah Aksi';
        $this->load->view('action/add',$data);
    }

    function edit($param=''){
        validate_ajax();
        $data['title'] = 'Edit Aksi';
        $data['row']   = $this->mglobal->selectDataByID($this->_table, 'action_id', decode($param));
        $data['id']    = $param;
        $this->load->view('action/edit',$data);
    }

    function action_add(){
        validate_ajax();
        $post   = $this->input->post();
        $action = strtolower($post['action_name']);

        /* validation */
        $this->form_validation->set_rules('action_name', 'Nama Aksi', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* data post */
        $data = array(
            'action_name' => $action,
        );

        $where = array('LOWER(action_name)' => $action);
        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($this->mglobal->checkData($this->_table, $where)){
            $response = json_api('failed','Nama Aksi Sudah Ada'); 
        }else{
            $save = $this->mglobal->saveData($this->_table, $data);
            if($save){
                $response = json_api('success','Simpan Data Berhasil');
            }else{
                $response = json_encode($this->db->error());       
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;    
    }

    function action_edit(){
        validate_ajax();
        $post   = $this->input->post();
        $id     = decode($post['id']);
        $action = strtolower($post['action_name']);

        /* validation */
        $this->form_validation->set_rules('action_name', 'Nama Aksi', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* data post */
        $data = array(
            'action_id'   => $id,
            'action_name' => $action
        );

        /* check group name */
        $where = array('LOWER(action_name)' => $action);
        $check = $this->mglobal->checkData($this->_table, $where, 'action_id', $id);

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($check){
            $response = json_api('failed','Nama Aksi Sudah Ada'); 
        }else{
            $update = $this->mglobal->updateData($this->_table, $data, 'action_id');
            if($update){
                $response = json_api('success','Update Data Berhasil');
            }else{
                $response = json_encode($this->db->error());
            }
        }    

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;   
    }

    function delete($param=''){
        validate_ajax();
        $delete = $this->mglobal->deleteDataById($this->_table,'action_id',decode($param));
        if($delete){
            $response =  json_api('success','Delete Data Berhasil');
        }else{
            $response =  json_encode($this->db->error()); 
        }    

        $data = array(
            'user_id'=> decode($param)
        );

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log); 

        echo $response;  
    }

    function get_list(){
    	validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->action_model->get_list();

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                $r->action_id = encode($r->action_id);
                $r->action    = createBtnAction($this->module,'edit',$r->action_id);
                $where        = array('action_id' => decode($r->action_id));
                $check        = $this->mglobal->checkData('t_mtr_menu_detail',$where);

                if(!$check){
                    $r->action .= createBtnAction($this->module,'delete',$r->action_id);
                }
                $rows_data[] = $r;
                unset($r->action_id);
            }
        }

        $data['total'] = $dataList['total'];
        $data['rows']  = $rows_data;
        
        echo json_encode($data);
    }
}
