<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * -----------------
 * CLASS NAME : Anggota
 * -----------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Anggota extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('anggota_model');
        $this->module = 'anggota';
        $this->_table = 't_mtr_anggota';
        $this->_sess  = $this->session->userdata('app_session');
        $this->_code  = decode($this->_sess['code']);
    }
 	
 	function index() {
 		$data['title']   = "Anggota";
 		$data['content'] = "anggota/index";
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');

        $this->load->view('template/page', $data);
    }

    function add(){
        validate_ajax();
        $data['title'] = 'Tambah Anggota';

        $this->load->view('Anggota/add',$data);
    }

    function edit($param=''){
        validate_ajax();
        $data['title']       = 'Edit Anggota';
        $data['Anggotagroup']   = dropdown_Anggota_group();
        $data['company']     = dropdown_company();
        $data['row']         = $this->mglobal->selectDataByID($this->_table, 'anggota_id', decode($param));
        $data['id']          = $param;
        $data['code']        = strtoupper($this->_code);
        $data['Anggotagroupid'] = encode($data['row']->Anggota_group);
        $data['companycode'] = encode($data['row']->company_code);
        $this->load->view('Anggota/edit',$data);
    }

    function change_password($param=''){
        validate_ajax();
        $data['title'] = 'Ganti Password';
        $data['row']   = $this->mglobal->selectDataByID($this->_table, 'anggota_id', decode($param));
        $data['id']    = $param;
        $this->load->view('Anggota/change_password',$data);
    }

    function action_add(){
        validate_ajax();
        $post = $this->input->post();

        /* validation */
        $this->form_validation
            ->set_rules('fullname', 'Nama Lengkap', 'trim|required')
            ->set_rules('Anggotaname', 'Anggotaname', 'trim|required')
            ->set_rules('password', 'Password', 'trim|required')
            ->set_rules('Anggotagroup', 'Anggota Grup', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        if(strtoupper($this->_code) == 'KSRRYT'){
            $code = decode($post['company']);
        }else{
            $code = $this->_code;
        }

        /* data post */
        $data = array(
            'full_name' => $post['fullname'], 
            'Anggota_name' => $post['Anggotaname'], 
            'password'  => password_hash($this->config->item('encryption_key') . $post['password'], PASSWORD_BCRYPT), 
            'Anggota_group'=> decode($post['Anggotagroup']), 
            'company_code'=> $code, 
        );

        $where = array('LOWER(Anggota_name)' => strtolower($post['Anggotaname']));
        $check = $this->mglobal->checkData($this->_table,$where);

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($check){
            $response =  json_api('failed','Anggotaname '.$post['Anggotaname'].' Sudah Ada'); 
        }else{
            $save = $this->mglobal->saveData($this->_table, $data);
            if($save){
                $response = json_api('success','Simpan Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }      

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;
    }

    function action_edit(){
        validate_ajax();
        $post = $this->input->post();

        /* validation */
        $this->form_validation
            ->set_rules('fullname', 'Nama Lengkap', 'trim|required')
            ->set_rules('Anggotagroup', 'Anggota Grup', 'trim|required')
            ->set_rules('company', 'Perusahaan', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        if(strtoupper($this->_code) == 'KSRRYT'){
            $code = decode($post['company']);
        }else{
            $code = $this->_code;
        }

        /* data post */
        $data = array(
            'anggota_id'   => decode($post['Anggotaid']),
            'full_name' => $post['fullname'], 
            'Anggota_group'=> decode($post['Anggotagroup']),
            'company_code'=> $code, 
        );

        if($this->form_validation->run() == FALSE){
            $response =  json_api('failed',validation_errors());
        }else{
            $update = $this->mglobal->updateData($this->_table, $data, 'anggota_id');
            if($update){
                $response =  json_api('success','Update Data Berhasil');
            }else{
                $response =  json_encode($this->db->error()); 
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;     
    }

    function action_change_password(){
        validate_ajax();
        $post = $this->input->post();

        /* validation */
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        /* check must change password */
        $must_change_password = $this->mglobal->selectDataByID($this->_table, 'anggota_id', decode($post['Anggotaid']))->must_change_password;

        /* data post */
        $data = array(
            'anggota_id'  => decode($post['Anggotaid']),
            'must_change_password' => (int)$must_change_password + 1,
            'password' => password_hash($this->config->item('encryption_key') . $post['password'], PASSWORD_BCRYPT)
        );

        if($this->form_validation->run() == FALSE){
            $response =  json_api('failed',validation_errors());
        }else{
            $change = $this->mglobal->updateData($this->_table, $data, 'anggota_id');
            if($change){
                $response =  json_api('success','Ganti Password Berhasil');
            }else{
                $response =  json_encode($this->db->error());
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;     
    }

    function delete($param=''){
        validate_ajax();
        $delete = $this->mglobal->deleteDataById($this->_table,'anggota_id',decode($param));
        if($delete){
            $response =  json_api('success','Delete Data Berhasil');
        }else{
            $response =  json_encode($this->db->error()); 
        }    

        $data = array(
            'anggota_id'=> decode($param)
        );

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log); 

        echo $response;  
    }

    function actived($param=''){
        validate_ajax();
        $exp = explode('|', decode($param));

        $data = array(
            'anggota_id'=> $exp[0],
            'active' => $exp[1]
        );

        $save = $this->mglobal->updateData($this->_table,$data,'anggota_id');
        if($save){
            $response = json_api('success','Berhasil diupdate');
        }else{
            $response = json_encode($this->db->error()); 
        }    

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);   

        echo $response;
    }

    function get_list(){
    	validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->anggota_model->get_list($this->_code);
        $active    = '<span class="label bg-green">Aktif</span>';
        $nonactive = '<span class="label bg-red">Tidak Aktif</span>';

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                $act0          = encode("{$r->anggota_id}|0");
                $act1          = encode("{$r->anggota_id}|1");
                $r->anggota_id    = encode($r->anggota_id);
                $r->active     = $r->active;
                $r->action     = createBtnAction($this->module,'edit',$r->anggota_id);

                if(checkAccess('anggota','change_status')){
                    if($r->active == 1){
                        $actActived= site_url('anggota/actived/').$act0;
                        $r->active = $active;
                        $r->action.= '<button onclick="confirmationAction(\'Apakah Anda Yakin Akan Menonaktifkan Anggota ini?\',\''.$actActived.'\')" class="btn btn-danger btn-icon btn-xs" title="Nonaktifkan"><i class="fa fa-close"></i></button> ';
                    }else{
                        $actActived = site_url('anggota/actived/').$act1 ;
                        $r->active  = $nonactive;
                        $r->action .= '<button onclick="confirmationAction(\'Apakah Anda Yakin Akan Mengaktifkan Anggota ini\',\''.$actActived.'\')" class="btn btn-success btn-icon btn-xs" title="Aktifkan"><i class="fa fa-check"></i></button> ';
                    }
                }

                $r->action .= createBtnAction($this->module,'delete',$r->anggota_id);

                $rows_data[]= $r;
                unset($r->anggota_id);
            }
        }

        $data['total'] = $dataList['total'];
        $data['rows']  = $rows_data;
        
        echo json_encode($data);
    }
}
