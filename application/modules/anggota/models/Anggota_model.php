<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * --------------------------
 * CLASS NAME : Anggota_model
 * --------------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Anggota_model extends CI_Model {
	function get_list($code) {
		$data 	 = array();
		$search  = trim(strtoupper($this->db->escape_like_str($this->input->post('search'))));
		$page 	 = $this->input->post('page') ? $this->input->post('page') : 1;
		$rows 	 = $this->input->post('rows') ? $this->input->post('rows') : 10;
		$offset  = ($page - 1) * $rows;
		$sort 	 = $this->input->post('sort') ? $this->input->post('sort') : 'nama';
		$order 	 = $this->input->post('order') ? strtoupper($this->input->post('order')) : 'ASC';

		$where 	 = "WHERE active IN (0,1)";
		
		if($search != ''){
			$where .= " AND (UPPER(nama) LIKE '%{$search}%' OR UPPER(nomor) LIKE '%{$search}%')";
		}

		$sql = "SELECT *
				FROM t_mtr_anggota
				{$where} ORDER BY {$sort} {$order}";
		
		$query 		= $this->db->query($sql);
		$total_rows = $query->num_rows();
		$sql 	   .= " LIMIT {$rows} OFFSET {$offset}";
		$query2		= $this->db->query($sql);
				
		$data['total'] = $total_rows;
		$data['rows']  = $query2->result();
		
		return $data;
	}
}