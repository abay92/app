<?php 

/**
 * ------------------------------
 * NAME FILE : add.php - form add
 * ------------------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

?>

<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('user/action_add', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Nama Lengkap</label>
                        <input type="text" name="fullname" class="form-control" placeholder="Nama Lengkap" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer text-right">
            <?php echo createBtnForm('Simpan') ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.mfp-wrap').removeAttr('tabindex')
        $('.select2').select2()
        $('.select2').change(function(){
            $(this).valid();
        })

        validateForm('#ff',function(url,data){
            postData(url,data);
        });
    })
</script>