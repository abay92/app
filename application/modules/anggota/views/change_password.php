<?php 

/**
 * ------------------------------------------------------
 * NAME FILE : change_password.php - form change password
 * ------------------------------------------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

?>

<div class="col-md-4 col-md-offset-4">
    <div class="box box-primary">
        <?php echo headerForm($title.' ('.$row->user_name.')') ?>
        <?php echo form_open('user/action_change_password', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Password Baru</label>
                        <input type="password" name="password" class="form-control focus" placeholder="Passwword Baru" required> 
                        <input type="hidden" name="userid" value="<?php echo $id; ?>">
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Ganti Password') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        validatorPassword()
        rules    = { password : { password : true }, };

        validateForm('#ff',function(url,data){
            data.password = CryptoJS.MD5(data.password).toString()
            postData(url,data);
        });
    })
</script>