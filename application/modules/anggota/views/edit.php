<?php 

/**
 * -------------------------------
 * NAME FILE : edit.php - form edit
 * -------------------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

?>

<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title.' ('.$row->user_name.')') ?>
        <?php echo form_open('user/action_edit', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Nama Lengkap</label>
                        <input type="text" name="fullname" class="form-control" placeholder="Full Name" required value="<?php echo $row->full_name ?>"> 
                        <input type="hidden" name="userid" value="<?php echo $id; ?>">
                    </div>
                    <div class="col-sm-6">
                        <label>User Grup</label>
                        <?php echo form_dropdown('usergroup', $usergroup, $usergroupid, 'class="form-control select2" required data-placeholder="Pilih User Grup"'); ?>
                    </div>
                </div>
            </div>
            <?php if($code== 'KSRRYT'){ ?>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Perusahaan</label>
                            <?php echo form_dropdown('company', $company, $companycode, 'class="form-control select2" required data-placeholder="Pilih Perusahaan"'); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <?php echo createBtnForm('Update') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.mfp-wrap').removeAttr('tabindex')
        $('.select2').select2()
        $('.select2').change(function(){
            $(this).valid();
        })

        validateForm('#ff',function(url,data){
            postData(url,data)
        });
    })
</script>