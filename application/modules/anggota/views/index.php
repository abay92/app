<?php 

/**
 * ---------------------------
 * NAME FILE : index.php -  list
 * ---------------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

?>

<div class="content-wrapper">
	<section class="content">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $title ?></h3>
                <div class="box-tools pull-right">
                    <?php echo $btn_add ?>
                </div>
			</div>

            <div id="tb">
                 <div class="col-md-3 col-md-offset-9 pad-top">
                    <div class="form-group mar-bottom">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Cari nama lengkap, username" id="search" autocomplete="off">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-flat" id="btnSearch">Cari</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-body no-padding">
                <table class="table" id="grid"></table>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        settingDefaultDatagrid()
        $('#grid').datagrid({
            url        : baseURL+'anggota/get_list',
            columns : [[
                { field: 'nomor', title: 'Nomor', width: 100, sortable  : true},
                { field: 'nama', title: 'Nama', width: 100, sortable  : true},
                { field: 'no_identita', title: 'Nomor Identitas', width: 100, sortable  : true},
                { field: 'tgl_masuk', title: 'Tanggal Masuk', width: 100, sortable  : true},
                { field: 'active', title: 'Status', width: 50, sortable  : true, align: 'center'},
                { field: 'action', title: 'Aksi', width: 100, align: 'center'}
            ]],

            onLoadSuccess: onLoadSuccessDatagrid,
        })

        $('#btnSearch').click(function(){
            searchingDatagrid()
        })

        $('#search').keydown(function(e) {
            if (e.which == 13){
                searchingDatagrid()
            }
        })
    })
</script>