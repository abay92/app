<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ------------------
 * CLASS NAME : Chat
 * ------------------
 *
 * @author     Robai <abayr92@gmail.com>
 * @copyright  2018
 *
 */

class Chat extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('chat_model');
        $this->module = 'chat';
        $this->_table = 't_mtr_user';
        $this->_sess  = $this->session->userdata('app_session');
        $this->_code  = decode($this->_sess['code']);
    }
 	
 	function index() {
 		$data['title']   = "Chatting";
 		$data['content'] = "chat/index";
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');

        $this->load->view('template/page', $data);
    }

    function add(){
        validate_ajax();
        $data['title']     = 'Tambah User';
        $data['usergroup'] = dropdown_user_group();
        $data['company']   = dropdown_company();
        $data['code']      = strtoupper($this->_code);

        $this->load->view('user/add',$data);
    }

    function edit($param=''){
        validate_ajax();
        $data['title']     = 'Edit User';
        $data['usergroup'] = dropdown_user_group();
        $data['company']   = dropdown_company();
        $data['row']       = $this->mglobal->selectData($this->_table, 'user_id', decode($param));
        $data['id']        = $param;
        $data['code']      = strtoupper($this->_code);
        $data['usergroupid'] = encode($data['row']->user_group);
        $data['companycode'] = encode($data['row']->company_code);
        $this->load->view('user/edit',$data);
    }

    function change_password($param=''){
        validate_ajax();
        $data['title']     = 'Ganti Password';
        $data['row']       = $this->mglobal->selectData($this->_table, 'user_id', decode($param));
        $data['id']        = $param;
        $this->load->view('user/change_password',$data);
    }

    function action_add(){
        validate_ajax();
        $post = $this->input->post();

        /* validation */
        $this->form_validation
            ->set_rules('fullname', 'Nama Lengkap', 'trim|required')
            ->set_rules('username', 'Username', 'trim|required')
            ->set_rules('password', 'Password', 'trim|required')
            ->set_rules('usergroup', 'User Grup', 'trim|required');

        if(strtoupper($this->_code) == 'KSRRYT'){
            $code = decode($post['company']);
        }else{
            $code = $this->_code;
        }

        /* data post */
        $data = array(
            'full_name' => $post['fullname'], 
            'user_name' => $post['username'], 
            'password'  => password_hash($this->config->item('encryption_key') . $post['password'], PASSWORD_BCRYPT), 
            'user_group'=> decode($post['usergroup']), 
            'company_code'=> $code, 
        );

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($this->mglobal->selectData($this->_table, 'user_name', $post['username'])){
            $response =  json_api('failed','Username Sudah Ada'); 
        }else{
            $save = $this->mglobal->saveData($this->_table, $data);
            if($save){
                $response = json_api('success','Simpan Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }      

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;
    }

    function action_edit(){
        validate_ajax();
        $post = $this->input->post();

        /* validation */
        $this->form_validation
            ->set_rules('fullname', 'Nama Lengkap', 'trim|required')
            ->set_rules('usergroup', 'User Grup', 'trim|required')
            ->set_rules('company', 'Perusahaan', 'trim|required');

        if(strtoupper($this->_code) == 'KSRRYT'){
            $code = decode($post['company']);
        }else{
            $code = $this->_code;
        }

        /* data post */
        $data = array(
            'user_id'   => decode($post['userid']),
            'full_name' => $post['fullname'], 
            'user_group'=> decode($post['usergroup']),
            'company_code'=> $code, 
        );

        if($this->form_validation->run() == FALSE){
            $response =  json_api('failed',validation_errors());
        }else{
            $update = $this->mglobal->updateData($this->_table, $data, 'user_id');
            if($update){
                $response =  json_api('success','Update Data Berhasil');
            }else{
                $response =  json_encode($this->db->error()); 
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;     
    }

    function action_change_password(){
        validate_ajax();
        $post = $this->input->post();

        /* validation */
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        /* check must change password */
        $must_change_password = $this->mglobal->selectData($this->_table, 'user_id', decode($post['userid']))->must_change_password;

        /* data post */
        $data = array(
            'user_id'  => decode($post['userid']),
            'must_change_password' => (int)$must_change_password + 1,
            'password' => password_hash($this->config->item('encryption_key') . $post['password'], PASSWORD_BCRYPT)
        );

        if($this->form_validation->run() == FALSE){
            $response =  json_api('failed',validation_errors());
        }else{
            $change = $this->mglobal->updateData($this->_table, $data, 'user_id');
            if($change){
                $response =  json_api('success','Ganti Password Berhasil');
            }else{
                $response =  json_encode($this->db->error()); 
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;     
    }

    function delete($param=''){
        validate_ajax();
        $delete = $this->mglobal->deleteDataById($this->_table,'user_id',decode($param));
        if($delete){
            $response =  json_api('success','Delete Data Berhasil');
        }else{
            $response =  json_encode($this->db->error()); 
        }    

        $data = array(
            'user_id'=> decode($param)
        );

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log); 

        echo $response;  
    }

    function actived($param='',$param2=''){
        validate_ajax();
        if($param == 0){
            $data = array(
                'user_id'=> decode($param2),
                'active' => $param,
                'is_login' => 0
            );
        }else{
            $data = array(
                'user_id'=> decode($param2),
                'active' => $param
            );
        }
        $save = $this->mglobal->updateData($this->_table,$data,'user_id');
        if($save){
            $response = json_api('success','Berhasil diupdate');
        }else{
            $response = json_encode($this->db->error());
        }    

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);   

        echo $response;
    }

    function get_list(){
    	validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->chat_model->get_list($this->_code,decode($this->_sess['user']));
        $url       = site_url('a');

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                // $r->user_id = encode($r->user_id);
                $r->active  = $r->active;
                $r->action  = '<button onclick="chat(\''.$r->user_id.'\')" class="btn btn-primary btn-icon btn-sm" title=""><i class="fa fa-wechat"></i> Chat</button>';

                $rows_data[]= $r;
            }
        }

        $data['total'] = $dataList['total'];
        $data['rows']  = $rows_data;
        
        echo json_encode($data);
    }

    public function getChats(){
        header('Content-Type: application/json');
        if ($this->input->is_ajax_request()) {
            // Find friend
            $friend = $this->db->get_where('t_mtr_user', array('user_id' => $this->input->post('chatWith')), 1)->row();
            // echo decode($this->input->post('chatWith'));exit;

            // Get Chats
            $chats = $this->db
                ->select('t_trx_chatting.*, t_mtr_user.full_name')
                ->from('t_trx_chatting')
                ->join('t_mtr_user', 't_trx_chatting.send_by = t_mtr_user.user_id')
                ->where('(send_by = '. decode($this->_sess['user']) .' AND send_to = '. $friend->user_id .')')
                ->or_where('(send_to = '. decode($this->_sess['user']) .' AND send_by = '. $friend->user_id .')')
                ->order_by('t_trx_chatting.time', 'desc')
                ->limit(100)
                ->get()
                ->result();

            $result = array(
                'name' => $friend->full_name,
                'chats' => $chats
            );
            echo json_encode($result);
        }
    }

    public function sendMessage(){
        $this->db->insert('t_trx_chatting', array(
            'message' => htmlentities($this->input->post('message', true)),
            'send_to' => $this->input->post('chatWith'),
            'send_by' => decode($this->_sess['user'])
        ));
    }
}
