<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Chat_model extends CI_Model {
	function get_list($code,$id) {
		$data 	 = array();
		$search  = trim(strtoupper($this->db->escape_like_str($this->input->post('search'))));
		$page 	 = $this->input->post('page') ? $this->input->post('page') : 1;
		$rows 	 = $this->input->post('rows') ? $this->input->post('rows') : 10;
		$offset  = ($page - 1) * $rows;
		$sort 	 = $this->input->post('sort') ? $this->input->post('sort') : 'full_name';
		$order 	 = $this->input->post('order') ? strtoupper($this->input->post('order')) : 'ASC';

		if(strtoupper($code) == 'KSRRYT'){
			$where 	 = "WHERE u.active = 1 AND user_id != $id";		
		}else{
			$where 	 = "WHERE u.active = 1 AND user_id != $id AND company_code='$code'";
		}
		
		if($search != ''){
			$where .= " AND (UPPER(full_name) LIKE '%{$search}%' OR UPPER(user_name) LIKE '%{$search}%')";
		}

		$sql = "SELECT u.user_id, u.full_name, g.group_name, u.active, c.name
				FROM t_mtr_user u
				LEFT JOIN t_mtr_user_group g ON g.group_id = u.user_group
				LEFT JOIN t_mtr_company c ON c.code = u.company_code
				{$where} ORDER BY {$sort} {$order}";
		
		$query 		= $this->db->query($sql);
		$total_rows = $query->num_rows();
		$sql 	   .= " LIMIT {$rows} OFFSET {$offset}";
		$query2		= $this->db->query($sql);
				
		$data['total'] = $total_rows;
		$data['rows']  = $query2->result();
		
		return $data;
	}
}