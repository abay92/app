<link href="<?php echo config_item('css'); ?>chat.css" rel="stylesheet">
<div class="content-wrapper">
	<section class="content">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $title ?></h3>
			</div>
            <div id="tb">
                <div class="col-md-3 col-md-offset-9 pad-top">
                    <div class="form-group mar-bottom">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Cari nama lengkap" id="search" autocomplete="off">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-flat" id="btnSearch">Cari</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-body no-padding">
                <table class="table" id="grid"></table>
            </div>
        </div>
    </section>
</div>
<div id="wgt-container-template" style="display: none">
    <div class="msg-wgt-container" style="right: 20px; z-index: 2">
        <div class="msg-wgt-header">
            <a href="javascript:;" class="online"></a>
            <a href="javascript:;" class="name"></a>
            <a href="javascript:;" class="close">x</a>
        </div>
        <div class="msg-wgt-message-container">
            <table width="100%" class="msg-wgt-message-list">
            </table>
        </div>
        <div class="msg-wgt-message-form">
            <textarea name="message" placeholder="Type your message. Press Shift + Enter for newline"></textarea>
        </div>
    </div>
</div>

<script type="text/x-template" id="msg-template" style="display: none">
    <tbody>
        <tr class="msg-wgt-message-list-header">
            <td rowspan="2"><img src="<?= base_url('assets/img/avatar5.png') ?>"></td>
            <td class="name"></td>
            <td class="time"></td>
        </tr>
        <tr class="msg-wgt-message-list-body">
            <td colspan="2"></td>
        </tr>
        <tr class="msg-wgt-message-list-separator"><td colspan="3"></td></tr>
    </tbody>
</script>

<script type="text/javascript">
    $(document).ready(function() {
        settingDefaultDatagrid()
        $('#grid').datagrid({
            url        : baseURL+'chat/get_list',
            columns : [[
            { field: 'full_name', title: 'Nama Lengkap', width: 100, sortable  : true},
            { field: 'action', title: '', width: 100, align: 'center'}
            ]],

            onLoadSuccess: onLoadSuccessDatagrid,
        })

        $('#btnSearch').click(function(){
            searchingDatagrid()
        })

        $('#search').keydown(function(e) {
            if (e.which == 13){
                searchingDatagrid()
            }
        })
    })

    var chatPosition = [
        false, // 1
        false, // 2
        false, // 3
        false, // 4
        false, // 5
        false, // 6
        false, // 7
        false, // 8
        false, // 9
        false // 10
    ];

    function chat(id){
        var $data = id;
        var posRight = 0;
        var position;
        for(var i in chatPosition) {
            if (chatPosition[i] == false) {
                posRight = (i * 270) + 20;
                chatPosition[i] = $data;
                position = i;
                break;
            }
        }
        
        var tpl = $('#wgt-container-template').html();
        var tplBody = $('<div/>').append(tpl);
        tplBody.find('.msg-wgt-container').addClass('msg-wgt-active');
        tplBody.find('.msg-wgt-container').css('right', posRight + 'px');
        tplBody.find('.msg-wgt-container').attr('data-chat-position', position);
        tplBody.find('.msg-wgt-container').attr('data-chat-with', $data);
        $('body').append(tplBody.html());
        initializeChat();
    }

    // Minimize Maximize
    $(document).on('click', '.msg-wgt-header > a.name', function() {
        var parent = $(this).parent().parent();
        if (parent.hasClass('minimize')) {
            parent.removeClass('minimize')
        } else {
            parent.addClass('minimize');
        }
    });

    // Close
    $(document).on('click', '.msg-wgt-header > a.close', function() {
        var parent = $(this).parent().parent();
        var $data = parent.data();
        parent.remove();
        chatPosition[$data.chatPosition] = false;
        setTimeout(function() {
            initializeChat();
        }, 1000)
    });

    var chatInterval = [];

    var initializeChat = function() {
        $.each(chatInterval, function(index, val) {
            clearInterval(chatInterval[index]);   
        });

        $('.msg-wgt-active').each(function(index, el) {
            var $data = $(this).data();
            var $that = $(this);
            var $container = $that.find('.msg-wgt-message-container');

            chatInterval.push(setInterval(function() {

                var oldscrollHeight = $container[0].scrollHeight;
                var oldLength = 0;
                $.post('<?= site_url('chat/getChats') ?>', {chatWith: $data.chatWith}, function(data, textStatus, xhr) {
                    $that.find('a.name').text(data.name);
                    // from last
                    var chatLength = data.chats.length;
                    var newIndex = data.chats.length;
                    $.each(data.chats, function(index, el) {
                        newIndex--;
                        var val = data.chats[newIndex];

                        var tpl = $('#msg-template').html();
                        var tplBody = $('<div/>').append(tpl);
                        var id = (val.chat_id +'_'+ val.send_by +'_'+ val.send_to).toString();
                        

                        if ($that.find('#'+ id).length == 0) {
                            tplBody.find('tbody').attr('id', id); // set class
                            tplBody.find('td.name').text(val.name); // set name
                            tplBody.find('td.time').text(val.time); // set time
                            tplBody.find('.msg-wgt-message-list-body > td').html(nl2br(val.message)); // set message
                            $that.find('.msg-wgt-message-list').append(tplBody.html()); // append message

                            //Auto-scroll
                            var newscrollHeight = $container[0].scrollHeight - 20; //Scroll height after the request
                            if (newIndex === 0) {
                                $container.animate({ scrollTop: newscrollHeight }, 'normal'); //Autoscroll to bottom of div
                            }
                        }
                    });
                });
            }, 1000));

            $that.find('textarea').on('keydown', function(e) {
                var $textArea = $(this);
                if (e.keyCode === 13 && e.shiftKey === false) {
                    $.post('<?= site_url('chat/sendMessage') ?>', {message: $textArea.val(), chatWith: $data.chatWith}, function(data, textStatus, xhr) {
                    });
                    $textArea.val(''); // clear input

                    e.preventDefault(); // stop 
                    return false;
                }
            });
        });
    }
    var nl2br = function(str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    }


    // on load
    initializeChat();
</script>