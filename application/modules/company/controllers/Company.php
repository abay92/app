<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * --------------------
 * CLASS NAME : Company
 * --------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Company extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('company_model');
        $this->module = 'company';
        $this->_table = 't_mtr_company';
    }
 	
 	function index() {
 		$data['title']   = "Perusahaan";
 		$data['content'] = "company/index";
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');

        $this->load->view('template/page', $data);
    }

    function add(){
        validate_ajax();
        $data['title']  = "Tambah Perusahaan";
        $this->load->view('company/add',$data);
    }

    function edit($param=''){
        validate_ajax();
        $data['title']   = "Edit Perusahaan";
        $data['row']     = $this->mglobal->selectDataByID($this->_table, 'company_id', decode($param));
        $data['id']      = $param;
        $this->load->view('company/edit',$data);
    }

    function action_add(){
        validate_ajax();
        $post   = $this->input->post();

        /* validation */
        $this->form_validation
        ->set_rules('name', 'Nama', 'trim|required')
        ->set_rules('phone', 'Nomor Telepon', 'trim|required')
        ->set_rules('address', 'Alamat', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* data post */
        $data = array(
            'code' => strtoupper(substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 3)),
            // 'code' => $post['code'],
            'name' => $post['name'],
            'phone' => $post['phone'],
            'address' => $post['address'],
        );

        // $where = array('LOWER(code)' => strtolower($post['code']));
        // $check = $this->mglobal->checkData($this->_table,$where);

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }

        // elseif($check){
        //     $response = json_api('failed','Kode Perusahaan '.$post['code'].' Sudah Ada'); 
        // }

        else{
            $save = $this->mglobal->saveData($this->_table, $data);
            if($save){
                $response = json_api('success','Simpan Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;     
    }

    function action_edit(){
        validate_ajax();
        $post   = $this->input->post();

        /* validation */
        $this->form_validation
        ->set_rules('name', 'Nama', 'trim|required')
        ->set_rules('phone', 'Nomor Telepon', 'trim|required')
        ->set_rules('address', 'Alamat', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');
        
        $id   = decode($post['id']);

        /* data post */
        $data = array(
            'company_id' => $id,
            // 'code' => substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 5),
            // 'code' => $post['code'],
            'name' => $post['name'],
            'phone' => $post['phone'],
            'address' => $post['address'],
        );

        // $where = array('LOWER(code)' => strtolower($post['code']));
        // $check = $this->mglobal->checkData($this->_table, $where, 'company_id', $id);

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }

        // elseif($check){
        //     $response =  json_api('failed','Kode Perusahaan '.$post['code'].' Sudah Ada'); 
        // }

        else{
            $update = $this->mglobal->updateData($this->_table, $data, 'company_id');

            if($update){
                $response = json_api('success','Update Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;
    }

    function delete($param=''){
        validate_ajax();
        $delete = $this->mglobal->deleteDataById($this->_table,'company_id',decode($param));
        if($delete){
            $response =  json_api('success','Delete Data Berhasil');
        }else{
            $response =  json_encode($this->db->error()); 
        }    

        $data = array(
            'company_id'=> decode($param)
        );

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log); 

        echo $response;  
    }

    function get_list(){
    	validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->company_model->get_list();

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                $r->company_id= encode($r->company_id);
                $r->action    = createBtnAction($this->module,'edit',$r->company_id);

                $where = array('company_code' => $r->code);
                $check = $this->mglobal->checkData('t_mtr_user',$where);

                if(!$check){
                     $r->action   .= createBtnAction($this->module,'delete',$r->company_id);
                }

                $rows_data[]= $r;
                unset($r->company_id);
            }
        }

        $data['total'] = $dataList['total'];
        $data['rows']  = $rows_data;
        
        echo json_encode($data);
    }
}
