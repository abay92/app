<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * --------------------------
 * CLASS NAME : Company_model
 * --------------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Company_model extends CI_Model {
	function get_list() {
		$data 	 = array();
		$search  = trim(strtoupper($this->db->escape_like_str($this->input->post('search'))));
		$year  	 = $this->input->post('year');
		$page 	 = $this->input->post('page') ? $this->input->post('page') : 1;
		$rows 	 = $this->input->post('rows') ? $this->input->post('rows') : 10;
		$offset  = ($page - 1) * $rows;
		$sort 	 = $this->input->post('sort') ? $this->input->post('sort') : 'name';
		$order 	 = $this->input->post('order') ? strtoupper($this->input->post('order')) : 'ASC';

		$where 	 = "WHERE active = 1";		
		
		if($search != ''){
			$where .= " AND (UPPER(code) LIKE '%{$search}%' OR UPPER(name) LIKE '%{$search}%' OR UPPER(address) LIKE '%{$search}%' OR UPPER(phone) LIKE '%{$search}%')";
		}

		$sql = "SELECT company_id, code, name, phone, address, active 
				FROM t_mtr_company
				{$where} ORDER BY {$sort} {$order}";
		
		$query 		= $this->db->query($sql);
		$total_rows = $query->num_rows();

		$sql 	   .= " LIMIT {$rows} OFFSET {$offset}";
		$query2		= $this->db->query($sql);
				
		$data['total'] = $total_rows;
		$data['rows']  = $query2->result();
		
		return $data;
	}
}