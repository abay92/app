<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('company/action_add', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                 <div class="row">
                   <!--  <div class="col-sm-4">
                        <label>Code</label>
                        <input type="text" name="code" class="form-control focus" placeholder="Kode Perusahaan" required>
                    </div> -->
                    <div class="col-sm-6">
                        <label>Nama</label>
                        <input type="text" name="name" class="form-control" placeholder="Nama Perusahaan" required>
                    </div>
                    <div class="col-sm-6">
                        <label>Telepon</label>
                        <input type="text" class="form-control my-form-control my-font-small" name="phone" placeholder="Nomor Telepon" required onkeypress="return isNumberKey(event)" minlength="10">
                    </div>
                </div>
            </div>
         <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Alamat</label>
                        <textarea name="address" class="form-control" rows="5" placeholder="Alamat" style="resize: none;"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Simpan') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $.validator.addMethod('number_phone', function (value) { 
            return /^([0])/.test(value); 
        }, 'Nomor yang Anda masukkan salah');
        rules = {
            phone : { number_phone : true }
        },

        validateForm('#ff',function(url,data){
           postData(url,data);
       });
    })
</script>