<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('company/action_edit', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                 <div class="row">
                    <!-- <div class="col-sm-4">
                        <label>Code</label>
                        <input type="text" name="code" class="form-control focus" placeholder="Kode Perusahaan" value="<?php //echo $row->code ?>" required>
                        <input type="hidden" name="id" value="<?php //echo $id ?>">
                    </div> -->
                    <div class="col-sm-6">
                        <label>Nama</label>
                        <input type="text" name="name" class="form-control" placeholder="Nama Perusahaan" value="<?php echo $row->name ?>" required>
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                    </div>
                    <div class="col-sm-6">
                        <label>Telepon</label>
                        <input type="text" class="form-control my-form-control my-font-small" name="phone" placeholder="Nomor Telepon" required onkeypress="return isNumberKey(event)" minlength="10" value="<?php echo $row->phone ?>">
                    </div>
                </div>
            </div>
         <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Alamat</label>
                        <textarea name="address" class="form-control" rows="5" placeholder="Alamat" style="resize: none;"><?php echo $row->address ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Simpan') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $.extend($.fn.datepicker.defaults, {
            format: 'dd MM yyyy',
            autoclose: true,
            orientation: 'auto bottom',
            daysOfWeekDisabled: [0,1,2,3,4,6]
        });

        $('#date').datepicker().on('changeDate', function(e) {
            $(this).valid();
        });

        $('#date').datepicker()
        $('.date').click(function (e) {
            $('#date').datepicker().focus();
            e.preventDefault();
        });

        validateForm('#ff',function(url,data){
           postData(url,data);
       });
    })
</script>