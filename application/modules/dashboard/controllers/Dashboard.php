<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ----------------------
 * CLASS NAME : Dashboard
 * ----------------------
 *
 * @author     Robai <abayr92@gmail.com>
 * @copyright  2018
 *
 */

class Dashboard extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('dashboard_model');
        $this->_sess  = $this->session->userdata('app_session');
        $this->_code  = $this->_sess['code'];
    }
 	
 	function index() {
 		$data['title']       = "Dashboard";
        $data['code']        = $this->_code;
 		$data['totalIncome'] = $this->dashboard_model->total_income(decode($this->_sess['code']));
 		$data['content']     = "dashboard/index";

        $this->load->view('template/page', $data);
    }
}
