<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	function total_income($code) {
		$date  	= date('Y');
		$sql  	= "SELECT * FROM t_trx_income WHERE active = 1 AND YEAR(date) = '$date' AND company_code = '$code'";
		$income =  $this->db->query($sql)->result();
        $total 	= 0;

        if($income){
        	foreach ($income as $key => $value) {
        		$total += $value->income;
        	}
        }

        return number_format($total,0,',','.');
	}
}