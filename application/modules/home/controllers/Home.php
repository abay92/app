<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ------------------
 * CLASS NAME : Home
 * ------------------
 *
 * @author     Robai <abayr92@gmail.com>
 * @copyright  2018
 *
 */

class Home extends MY_Controller {
 
    function __construct() {
        parent::__construct();
    }
 	
 	function index() {
 		$data['title']   = "Home";
 		$data['content'] = "home/index";
    
        $this->load->view('template/page', $data);
    }
}
