<style type="text/css">
	h3 {
	    margin-top: 5px;
	    margin-bottom: 0px;
	}
</style>
<div class="content-wrapper">
	<!-- <center><h3><b><?php echo ucwords(strtolower(config_item('title_web'))) ; ?></b></h3></center> -->
	<section class="content">
		<div class="col-md-12">
          <div class="box box-solid">
            <div class="box-body">
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                	<?php 
					$foto = array('home1','home2','home3');
					foreach($foto as $key => $val) { 
						$active = '';
						if($key == 0){
							$active = 'class="active"';
						}
						?>
						<li data-target="#myCarousel" data-slide-to="<?php echo $key ?>" class="<?php echo $active ?>"></li>
					<?php } ?>
                </ol>
                
                <div class="carousel-inner">
                	<?php
					foreach($foto as $key => $val) { 
						$active = '';
						if($key == 0){
							$active = 'active';
						}
						?>
						<div class="item <?php echo $active ?>">
							<img src="<?php echo base_url() ?>assets/img/home/<?php echo $val ?>.jpg" class="img-css">
						</div>			    		
					<?php } ?>
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		height 	= window.screen.availHeight / 1.5;
		$('.img-css').css({
			'width': '100%', 
			'height': height+'px'
		});
	})
</script>