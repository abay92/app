<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * -------------------
 * CLASS NAME : Income
 * -------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Income extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('income_model');
        $this->module = 'income';
        $this->_table = 't_trx_income';
        $this->_sess  = $this->session->userdata('app_session');
        $this->_code  = decode($this->_sess['code']);
    }
 	
 	function index() {
 		$data['title']   = "Pemasukan";
 		$data['content'] = "income/index";
        $data['year']    = $this->income_model->list_year($this->_code);
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');

        $this->load->view('template/page', $data);
    }

    function add(){
        validate_ajax();
        $data['title']       = 'Tambah Pemasukan';
        $data['date']        = date('d F Y');
        $data['friday']      = date('d F Y', strtotime('previous friday') );
        $data['income_type'] = $this->income_model->dropdown_income_type();

        $this->load->view('income/add',$data);
    }

    function edit($param=''){
        validate_ajax();
        $data['title']       = 'Edit Pemasukan';
        $data['income_type'] = $this->income_model->dropdown_income_type();
        $data['row']         = $this->mglobal->selectDataByID($this->_table, 'income_id', decode($param));
        $data['id']          = $param;
        $data['incomeType']  = $data['row']->income_type;
        $data['date']        = date('d F Y', strtotime($data['row']->date));
        $data['friday']      = date('d F Y', strtotime('previous friday') );
        $data['income']      = number_format($data['row']->income,0,',','.');
        $this->load->view('income/edit',$data);
    }

    function action_add(){
        validate_ajax();
        $post   = $this->input->post();

        /* validation */
        $this->form_validation
        ->set_rules('income_type', 'Jenis Pemasukan', 'trim|required')
        ->set_rules('income', 'Pemasukan', 'trim|required')
        ->set_rules('date', 'Tanggal', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* data post */
        $data = array(
            'income_type' => $post['income_type'],
            'income' => $post['income'],
            'date' => date('Y-m-d', strtotime($post['date'])),
            'note' => $post['note'],
            'company_code' => $this->_code
        );

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }else{
            $save = $this->mglobal->saveData($this->_table, $data);
            if($save){
                $response = json_api('success','Simpan Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;     
    }

    function action_edit(){
        validate_ajax();
        $post   = $this->input->post();

        /* validation */
        $this->form_validation
        ->set_rules('income_type', 'Jenis Pemasukan', 'trim|required')
        ->set_rules('income', 'Pemasukan', 'trim|required')
        ->set_rules('date', 'Tanggal', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* data post */
        $data = array(
            'income_id'   => decode($post['id']),
            'income_type' => $post['income_type'],
            'income' => $post['income'],
            'date' => date('Y-m-d', strtotime($post['date'])),
            'note' => $post['note'],
            'company_code' => $this->_code
        );

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }else{
            $update = $this->mglobal->updateData($this->_table, $data, 'income_id');

            if($update){
                $response = json_api('success','Update Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;
    }

    function delete($param=''){
        validate_ajax();
        $delete = $this->mglobal->deleteDataById($this->_table,'income_id',decode($param));
        if($delete){
            $response =  json_api('success','Delete Data Berhasil');
        }else{
            $response =  json_encode($this->db->error()); 
        }    

        $data = array(
            'income_id'=> decode($param)
        );

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log); 

        echo $response;  
    }

    function get_list(){
    	validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->income_model->get_list($this->_code);

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                $r->income_id = encode($r->income_id);
                $r->date      = date_indo($r->date);
                $r->income    = number_format($r->income,0,',','.');
                $r->action    = createBtnAction($this->module,'edit',$r->income_id).' ';
                $r->action   .= createBtnAction($this->module,'delete',$r->income_id);

                $rows_data[]  = $r;
                unset($r->income_id);       
            }
        }

        if($dataList['income']){
            $saldo = $dataList['income'] - $dataList['spending'];
            $data['spending'] = number_format($dataList['spending'],0,',','.');

        }else{
            $saldo = 0;
            $data['spending'] = number_format(0,0,',','.');
        }

        $data['total']    = $dataList['total'];
        $data['rows']     = $rows_data;
        $data['income']   = number_format($dataList['income'],0,',','.');
        $data['saldo']    = number_format($saldo,0,',','.');;
        
        echo json_encode($data);
    }
}
