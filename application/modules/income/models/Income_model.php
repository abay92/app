<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Income_model extends CI_Model {
	function get_list($code) {
		$data 	 = array();
		$search  = trim(strtoupper($this->db->escape_like_str($this->input->post('search'))));
		$year  	 = $this->input->post('year');
		$page 	 = $this->input->post('page') ? $this->input->post('page') : 1;
		$rows 	 = $this->input->post('rows') ? $this->input->post('rows') : 10;
		$offset  = ($page - 1) * $rows;
		$sort 	 = $this->input->post('sort') ? $this->input->post('sort') : 'date';
		$order 	 = $this->input->post('order') ? strtoupper($this->input->post('order')) : 'DESC';

		$where 	 	   = "WHERE i.active = 1 AND YEAR(date) = '{$year}' AND company_code = '$code'";
		$whereSpending = "AND company_code = '$code'";
		
		if($search != ''){
			$where .= " AND (UPPER(lookup_desc) LIKE '%{$search}%' OR UPPER(note) LIKE '%{$search}%')";
		}

		$sql = "SELECT income_id, income_type, income, note, i.active, lookup_desc AS income_type_name, date 
			FROM t_trx_income i
			LEFT JOIN t_mtr_lookup l ON l.lookup_value = i.income_type AND LOWER(lookup_key) = 'income_type' 
			{$where} ORDER BY {$sort} {$order}";

		$sql_spending = "SELECT spending FROM t_trx_spending WHERE YEAR(date) = '{$year}' {$whereSpending}";
		
		$query 		= $this->db->query($sql);
		$total_rows = $query->num_rows();
		$income 	= 0;
		$spending 	= 0;

		foreach ($this->db->query($sql)->result() as $key => $value) {
			$income += $value->income;
		}

		foreach ($this->db->query($sql_spending)->result() as $key => $value) {
			$spending += $value->spending;
		}

		$sql 	   .= " LIMIT {$rows} OFFSET {$offset}";
		$query2		= $this->db->query($sql);
				
		$data['total'] = $total_rows;
		$data['income'] = $income;
		$data['spending'] = $spending;
		$data['rows']  = $query2->result();
		
		return $data;
	}

	function dropdown_income_type(){
		$sql 		= "SELECT lookup_value, lookup_desc FROM t_mtr_lookup WHERE LOWER(lookup_key) = 'income_type'";
        $query 		= $this->db->query($sql)->result();
        $data 		= array();
        $data[''] 	= '';

        if($query){
        	foreach($query as $row){
	         	$data[$row->lookup_value] = $row->lookup_desc;
	        }
        }

        return $data;
    }

    function list_year($code){
    	$sql = "SELECT DISTINCT SUBSTRING_INDEX(date, '-', 1) AS year 
			   	FROM t_trx_income 
			    WHERE company_code = '$code' 
			    ORDER BY date ASC";	
		
        $query 		= $this->db->query($sql)->result();
        $year 		= date('Y');
        $data[$year]= $year;

        if($query){
        	foreach($query as $row){
        		if($row->year != $year){
	         		$data[$row->year] = $row->year;
        		}
	        }
        }

        return $data;
    }
}