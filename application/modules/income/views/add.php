<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('income/action_add', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Jenis Pemasukan</label>
                        <?php echo form_dropdown('income_type', $income_type, '', 'class="form-control select2" required data-placeholder="Pilih Jenis Pemasukan"'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                 <div class="row">
                    <div class="col-sm-6">
                        <label>Nilai Pemasukan (Rp)</label>
                        <input type="text" pattern="[0-9]*" name="income" class="form-control income" placeholder="Nilai Pemasukan" required>
                    </div>
                    <div class="col-sm-6">
                        <label>Tanggal</label>
                        <div class="input-group">
                            <input type="text" name="date" class="form-control pull-right" id="date" value="<?php echo $date; ?>" required>
                            <div class="input-group-addon date" style="cursor: pointer">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Catatan</label>
                        <textarea name="note" class="form-control" rows="5" placeholder="Catatan" style="resize: none;"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Simpan') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $.extend($.fn.datepicker.defaults, {
            format: 'dd MM yyyy',
            endDate: new Date(),
            autoclose: true,
            orientation: 'auto bottom'
        });

        $('.mfp-wrap').removeAttr('tabindex')
        $('.select2').select2()
        $('.select2').change(function(){
            $(this).valid();
            if(this.value == '1'){
                $('#date').datepicker('setDaysOfWeekDisabled', [0,1,2,3,4,6])
                $('#date').datepicker('setDate', '<?php echo $friday; ?>')
            }else{
                $('#date').datepicker('setDaysOfWeekDisabled', 0)
                $('#date').datepicker('setDate', '<?php echo $date; ?>')
            }
        });

        $('#date').datepicker().on('changeDate', function(e) {
            $(this).valid();
        });

        $('#date').datepicker()
        $('.date').click(function (e) {
            $('#date').datepicker().focus();
            e.preventDefault();
        });

        $('.income').keyup(function(e){
            this.value = formatRupiah(this.value);
        })

        rules = {};
        validateForm('#ff',function(url,data){
           data.income = removeRupiah(data.income)
           postData(url,data);
       });
    })
</script>