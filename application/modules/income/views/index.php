<div class="content-wrapper">
	<section class="content">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $title ?></h3>
                <div class="box-tools pull-right">
                    <?php echo $btn_add ?>
                </div>
            </div>
            <div id="tb">
                <div class="col-md-2 pad-top">
                    <div class="form-group mar-bottom">
                        <?php echo form_dropdown('', $year, '', 'class="form-control input-sm" id="year" style="border-radius: 3px;"'); ?>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-7 pad-top">
                    <div class="form-group mar-bottom">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Cari jenis pemasukan, catatan" id="search" autocomplete="off">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-flat" id="btnSearch">Cari</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12"><hr></div>
                <div class="col-md-4">
                    <h4 id="incomes">Pemasukan Rp. 0,-</h4>
                </div>
                <div class="col-md-4">
                    <h4 id="spendings">Pengeluaran Rp. 0,-</h4>
                </div>
                <div class="col-md-4">
                    <h4 id="saldo">Total Saldo Rp. 0,-</h4>
                </div>
            </div>

            <!-- <div id="tb">
                <div class="padd-tb">

                </div>
                <div class="padd-tb">
                    <div class="col-md-7"></div>
                    <div class="col-md-2">
                        <?php echo form_dropdown('', $year, '', 'class="form-control input-sm" id="year" style="border-radius: 3px;"'); ?>
                    </div>
                    <div class="input-group input-group-sm col-md-3 col-md-offset-9">
                        <input type="text" class="form-control" placeholder="Cari jenis pemasukan, catatan" id="search" autocomplete="off">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-primary btn-flat" id="btnSearch">Cari</button>
                        </span>
                    </div>
                </div>
            </div> -->
            <div class="box-body no-padding">
                <table class="table" id="grid"></table>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        settingDefaultDatagrid()
        $('#grid').datagrid({
            url     : baseURL+'income/get_list',
            queryParams : {year : $('#year').val()},
            columns : [[
            { field: 'income_type_name', title: 'Jenis Pemasukan', width: 100, sortable  : true},
            { field: 'income', title: 'Nilai Pemasukan (Rp)', width: 100, sortable  : true},
            { field: 'date', title: 'Tanggal', width: 100, sortable  : true},
            { field: 'note', title: 'Catatan', width: 100},
            { field: 'action', title: 'Aksi', width: 100, align: 'center'}
            ]],

            onLoadSuccess: function(data){
                $('#grid').datagrid('resize');
                $('.sidebar-toggle').click(function(){
                    setTimeout(function(){
                        $('#grid').datagrid('resize');
                    },400)
                });

                $(window).resize(function(){
                    setTimeout(function(){
                        $('#grid').datagrid('resize');
                    },400)
                })

                $('#incomes').html('Pemasukan Rp. '+data.income+',-')
                $('#spendings').html('Pengeluaran Rp. '+data.spending+',-')
                $('#saldo').html('Total Saldo Rp. '+data.saldo+',-')
            },
        })

        $('#btnSearch').click(function(){
            searchingDatagrid()
        })

        $('#year').change(function(){
            searchingDatagrid()
        })

        $('#search').keydown(function(e) {
            if (e.which == 13){
                searchingDatagrid()
            }
        })

        function searchingDatagrid(){
            $('#grid').datagrid('load',{
                search: $('#search').val(),
                year  : $('#year').val(),
            });
        }
    })
</script>