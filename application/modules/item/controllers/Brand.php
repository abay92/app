<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * -------------------
 * CLASS NAME : Action
 * -------------------
 *
 * @author     Robai <abayr92@gmail.com>
 * @copyright  2018
 *
 */

class Brand extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('brand_model');
        $this->module = 'item/brand';
        $this->_table = 't_mtr_item_brand';
    }
 	
 	function index() {
 		$data['title']   = "Merk";
 		$data['content'] = "brand/index";
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');

        $this->load->view('template/page', $data);
    }

    function add(){
        validate_ajax();
        $data['title'] = 'Tambah Merk';
        $this->load->view('brand/add',$data);
    }

    function edit($param=''){
        validate_ajax();
        $data['title'] = 'Edit Merk';
        $data['row']   = $this->mglobal->selectDataByID($this->_table, 'brand_id', decode($param));
        $data['id']    = $param;
        $this->load->view('brand/edit',$data);
    }

    function action_add(){
        validate_ajax();
        $post = $this->input->post();
        $code = strtoupper($post['code']);
        $name = ucwords(strtolower($post['name']));

        /* validation */
        $this->form_validation->set_rules('code', 'Kode Merk', 'trim|required');
        $this->form_validation->set_rules('name', 'Nama Merk', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi.!');

        /* data post */
        $data = array(
            'brand_code' => $code,
            'brand_name' => $name,
        );

        $where = array(
            strtoupper('brand_code') => $code
        );

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($this->mglobal->checkData($this->_table, $where)){
            $response = json_api('failed','Kode Merk '.$post['code'].' Sudah Ada'); 
        }else{
            $save = $this->mglobal->saveData($this->_table, $data);
            if($save){
                $response = json_api('success','Simpan Data Berhasil');
            }else{
                $response = json_encode($this->db->error());       
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;    
    }

    function action_edit(){
        validate_ajax();
        $post = $this->input->post();
        $id   = decode($post['id']);
        $code = strtoupper($post['code']);
        $code_old = strtoupper($post['code_old']);
        $name = ucwords(strtolower($post['name']));

        /* validation */
        $this->form_validation->set_rules('code', 'Kode Merk', 'trim|required');
        $this->form_validation->set_rules('name', 'Nama Merk', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi.!');

        /* data post */
        $data = array(
            'brand_id'   => $id,
            'brand_code' => $code,
            'brand_name' => $name
        );

        /* check group name */
        $where = array(
            strtoupper('brand_code') => $code
        );

        $check = $this->mglobal->checkData($this->_table, $where, 'brand_id', $id);

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($check){
            $response = json_api('failed','Kode Merk '.$post['code'].' Sudah Ada'); 
        }else{
            $where = array(
                strtoupper('brand_code') => $code_old
            );

            $check = $this->mglobal->checkData('t_mtr_item', $where);
            if($check){
                if($code != $code_old){
                    $dataU = array(
                        'brand_code' => $code
                    );
                    $this->mglobal->update_data_by_not_id('t_mtr_item', $dataU, 'brand_code', $code_old);
                }
            }

            $update = $this->mglobal->updateData($this->_table, $data, 'brand_id');
            if($update){
                $response = json_api('success','Update Data Berhasil');
            }else{
                $response = json_encode($this->db->error());
            }
        }    

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;   
    }

    function delete($param=''){
        validate_ajax();
        $delete = $this->mglobal->deleteDataById($this->_table,'brand_id',decode($param));
        if($delete){
            $response =  json_api('success','Delete Data Berhasil');
        }else{
            $response =  json_encode($this->db->error()); 
        }    

        $data = array(
            'brand_id'=> decode($param)
        );

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log); 

        echo $response;  
    }

    function get_list(){
    	validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->brand_model->get_list();

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                $r->brand_id = encode($r->brand_id);
                $r->action   = createBtnAction($this->module,'edit',$r->brand_id);

                $where = array('brand_code' => $r->brand_code);
                $check = $this->mglobal->checkData('t_mtr_item',$where);

                if(!$check){
                    $r->action  .= createBtnAction($this->module,'delete',$r->brand_id);
                }

                $rows_data[] = $r;
                unset($r->brand_id);
            }
        }

        $data['total'] = $dataList['total'];
        $data['rows']  = $rows_data;
        
        echo json_encode($data);
    }
}
