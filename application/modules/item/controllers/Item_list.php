<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ----------------------
 * CLASS NAME : Item_list
 * ----------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Item_list extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('item_model');
        $this->module = 'item/item_list';
        $this->_table = 't_mtr_item';
        $this->_sess  = $this->session->userdata('app_session');
        $this->_name  = $this->_sess['fullname'];
    }
 	
 	function index() {
 		$data['title']   = "Barang";
 		$data['content'] = "list/index";
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');
        $data['btn_upload'] = createBtnAction($this->uri->uri_string(),'upload');
        $data['btn_download'] = createBtnAction($this->uri->uri_string(),'download');

        $this->load->view('template/page', $data);
    }

    function add(){
        validate_ajax();
        $data['title']    = 'Tambah Barang';
        $data['brand']    = json_encode($this->dropdown_brand());
        $data['category'] = json_encode($this->dropdown_category());
        $this->load->view('list/add',$data);
    }

    function edit($param=''){
        validate_ajax();

        $dataBrand    = array();
        $dataCategory = array();
        foreach ($this->dropdown_brand() as $key => $value) {
            $dataBrand[$value['id']] = $value['text'];
        }
        foreach ($this->dropdown_category() as $key => $value) {
            $dataCategory[$value['id']] = $value['text'];
        }

        $data['title']    = 'Edit Barang';
        $data['brand']    = $dataBrand;
        $data['category'] = $dataCategory;
        $data['row']      = $this->mglobal->selectDataByID($this->_table, 'item_id', decode($param));
        $data['id']       = $param;
        $data['stock']    = number_format($data['row']->stock,0,',','.');
        $data['price']    = number_format($data['row']->price,0,',','.');

        $this->load->view('list/edit',$data);
    }

    function action_add(){
        validate_ajax();
        $post = $this->input->post();
        $no   = 0;
        $row  = $no + 1;
        $data = array();

        /* validation */
        foreach($post['code'] as $code){
            $this->form_validation->set_rules('code['.$no.']', 'Kode Barang #'.($row), 'trim|required');
            $this->form_validation->set_rules('name['.$no.']', 'Nama Barang #'.($row), 'trim|required');
            $this->form_validation->set_rules('category['.$no.']', 'Kategory #'.($row), 'trim|required');
            $this->form_validation->set_rules('brand['.$no.']', 'Merk #'.($row), 'trim|required');
            $this->form_validation->set_rules('stock['.$no.']', 'Stok #'.($row), 'trim|required');
            $this->form_validation->set_rules('price['.$no.']', 'Harga #'.($row), 'trim|required');

            $data[] = array(
                'item_code' => $post['code'][$no], 
                'item_name' => $post['name'][$no], 
                'category_code' => decode($post['category'][$no]), 
                'brand_code' => decode($post['brand'][$no]), 
                'stock' => str_replace(".", "", $post['stock'][$no]), 
                'price' => str_replace(".", "", $post['price'][$no]), 
                'note' => $post['note'][$no]
            );

            $no++;
        }

        $this->form_validation->set_message('required','%s harus diisi.!');

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }else{
            $save = $this->mglobal->insertBatch($this->_table, $data);
            if($save){
                $response = json_api('success','Simpan Data Berhasil');
            }else{
                $response = json_encode($this->db->error());       
            }
        } 

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);
        echo $response;
    }

    function action_edit(){
        validate_ajax();
        $post = $this->input->post();
        $id   = decode($post['id']);
        $code = strtolower($post['code']);

        /* validation */
        $this->form_validation
        ->set_rules('code', 'Kode Barang', 'trim|required')
        ->set_rules('name', 'Nama Barang', 'trim|required')
        ->set_rules('category', 'Kategori', 'trim|required')
        ->set_rules('brand', 'Merk', 'trim|required')
        ->set_rules('stock', 'Stok', 'trim|required')
        ->set_rules('price', 'Harga', 'trim|required')
        ->set_rules('name', ' Barang', 'trim|required');

        $this->form_validation->set_message('required','%s harus diisi.!');

        /* data post */
        $data = array(
            'item_id'   => $id,
            'item_code' => $post['code'],
            'item_name' => $post['name'],
            'category_code' => decode($post['category']), 
            'brand_code' => decode($post['brand']), 
            'stock' => str_replace(".", "", $post['stock']), 
            'price' => str_replace(".", "", $post['price']), 
            'note' => $post['note']
        );

        /* check group name */
        $where = array('LOWER(item_code)' => $code);
        $check = $this->mglobal->checkData($this->_table, $where, 'item_id', $id);

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($check){
            $response = json_api('failed','Kode Barang Sudah Ada'); 
        }else{
            $update = $this->mglobal->updateData($this->_table, $data, 'item_id');
            if($update){
                $response = json_api('success','Update Data Berhasil');
            }else{
                $response = json_encode($this->db->error());
            }
        }    

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;   
    }

    function delete($param=''){
        validate_ajax();
        $delete = $this->mglobal->deleteDataById($this->_table,'item_id',decode($param));
        if($delete){
            $response =  json_api('success','Delete Data Berhasil');
        }else{
            $response =  json_encode($this->db->error()); 
        }    

        $data = array(
            'item_id'=> decode($param)
        );

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log); 

        echo $response;  
    }

    function get_list(){
    	validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->item_model->get_list();

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                $r->item_id = encode($r->item_id);
                $r->stock   = number_format($r->stock,0,',','.');
                $r->price   = number_format($r->price,0,',','.');
                $r->action  = createBtnAction($this->module,'edit',$r->item_id);
                $r->action .= createBtnAction($this->module,'delete',$r->item_id);

                $rows_data[]= $r;
                unset($r->item_id);
            }
        }

        $data['total'] = $dataList['total'];
        $data['rows']  = $rows_data;
        
        echo json_encode($data);
    }

    function upload(){
        validate_ajax();
        $data['title']    = 'Tambah Barang';
        $data['brand']    = json_encode($this->dropdown_brand());
        $data['category'] = json_encode($this->dropdown_category());
        $this->load->view('list/upload',$data);
    }

    function download(){
        // validate_ajax();
        $result = $this->item_model->download();

        if($result){
            foreach ($result as $row) {
                foreach ($row as $key => $value) {
                    $header[] = $key;
                }
            }

            $arr = array(
                array_unique($header)
            );

            foreach ($result as $row) {
                $arr[] = array(
                    $row->item_code,
                    $row->item_name,
                    $row->category_name,
                    $row->brand_name,
                    (int) $row->stock,
                    (int) $row->price,
                    $row->note,
                );
            }

            $data = array(
                'download' => $arr,
                'title'    => 'item_list_'.date('Y-m-d h:i:s'),
                'subject'  => 'item',
                'author'   => $this->_name,
                'sheet_name' => 'item',
                'file_name' => 'item_list_'.date('Y-m-d')
            );

            $response =  json_api(
                'success',
                'Download sukses',
                $data
            );

        }else{
            $response =  json_encode($this->db->error()); 
        }    

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($this->input->post()),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log); 

        echo $response;
    }

    function dropdown_brand(){
        $datas = $this->mglobal->selectAll('t_mtr_item_brand');
        $arr[] = array('id' => '', 'text' => '');
        $arr2  = array();

        if($datas){
            foreach ($datas as $key => $value) {
                $arr2[$key]['id']   = encode($value->brand_code);
                $arr2[$key]['text'] = $value->brand_name;
            }
        }

        return array_merge($arr,$arr2);
    }

    function dropdown_category(){   
        $datas = $this->mglobal->selectAll('t_mtr_item_category');
        $arr[] = array('id' => '', 'text' => '');
        $arr2  = array();

        if($datas){
            foreach ($datas as $key => $value) {
                $arr2[$key]['id']   = encode($value->category_code);
                $arr2[$key]['text'] = $value->category_name;
            }
        }

        return array_merge($arr,$arr2);
    }
}
