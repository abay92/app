<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends CI_Model {
	function get_list() {
		$data 	 = array();
		$search  = trim(strtoupper($this->db->escape_like_str($this->input->post('search'))));
		$page 	 = $this->input->post('page') ? $this->input->post('page') : 1;
		$rows 	 = $this->input->post('rows') ? $this->input->post('rows') : 10;
		$offset  = ($page - 1) * $rows;
		$sort 	 = $this->input->post('sort') ? $this->input->post('sort') : 'category_name';
		$order 	 = $this->input->post('order') ? strtoupper($this->input->post('order')) : 'ASC';

		$where 	 = "WHERE active IN (0,1)";		
		
		if($search != ''){
			$where .= " AND (UPPER(category_name) LIKE '%{$search}%')";
		}

		$sql = "SELECT category_id, category_code, category_name FROM t_mtr_item_category {$where} ORDER BY {$sort} {$order}";
		
		$query 		= $this->db->query($sql);
		$total_rows = $query->num_rows();
		$sql 	   .= " LIMIT {$rows} OFFSET {$offset}";
		$query2		= $this->db->query($sql);
				
		$data['total'] = $total_rows;
		$data['rows']  = $query2->result();
		
		return $data;
	}
}