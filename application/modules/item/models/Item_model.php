<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * -----------------------
 * CLASS NAME : Item_model
 * -----------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Item_model extends CI_Model {
	function get_list() {
		$data 	 = array();
		$search  = trim(strtoupper($this->db->escape_like_str($this->input->post('search'))));
		$page 	 = $this->input->post('page') ? $this->input->post('page') : 1;
		$rows 	 = $this->input->post('rows') ? $this->input->post('rows') : 10;
		$offset  = ($page - 1) * $rows;
		$sort 	 = $this->input->post('sort') ? $this->input->post('sort') : 'item_code';
		$order 	 = $this->input->post('order') ? strtoupper($this->input->post('order')) : 'ASC';

		$where 	 = "WHERE i.active = 1";		
		
		if($search != ''){
			$where .= " AND (UPPER(item_code) LIKE '%{$search}%' OR UPPER(item_name) LIKE '%{$search}%' OR UPPER(category_name) LIKE '%{$search}%' OR UPPER(brand_name) LIKE '%{$search}%')";
		}

		$sql = "SELECT item_id, item_code, item_name, stock, price, category_name, brand_name, note, i.category_code, i.brand_code
				FROM t_mtr_item i
				LEFT JOIN t_mtr_item_category c ON c.category_code = i.category_code AND c.active = 1
				LEFT JOIN t_mtr_item_brand b ON b.brand_code = i.brand_code AND b.active = 1
				{$where} ORDER BY {$sort} {$order}";
		
		$query 		= $this->db->query($sql);
		$total_rows = $query->num_rows();
		$sql 	   .= " LIMIT {$rows} OFFSET {$offset}";
		$query2		= $this->db->query($sql);
				
		$data['total'] = $total_rows;
		$data['rows']  = $query2->result();
		
		return $data;
	}

	function download() {
		$sql = "SELECT item_code, item_name, category_name, brand_name, stock, price, note
				FROM t_mtr_item i
				LEFT JOIN t_mtr_item_category c ON c.category_code = i.category_code AND c.active = 1
				LEFT JOIN t_mtr_item_brand b ON b.brand_code = i.brand_code AND b.active = 1
				WHERE i.active = 1";
		$query = $this->db->query($sql);		
		return $query->result();
	}
}