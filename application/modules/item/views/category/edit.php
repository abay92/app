<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('item/category/action_edit', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Kode Kategori</label>
                        <input type="text" name="code" class="form-control focus" placeholder="Kode Merk" required value="<?php echo $row->category_code ?>">
                    </div>
                    <div class="col-sm-9">
                        <label>Nama Kategori</label>
                        <input type="text" name="name" class="form-control" placeholder="Nama Merk" required value="<?php echo $row->category_name ?>">
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                        <input type="hidden" name="code_old" value="<?php echo $row->category_code ?>">
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Update') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        validateForm('#ff',function(url,data){
           postData(url,data);
        });
    })
</script>