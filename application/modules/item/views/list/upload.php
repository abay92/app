<div class="col-md-10 col-md-offset-1">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('item/item_list/action_add', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <table class="table table-bordered" id="addItem">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="110px">Kode Barang</th>
                        <th>Nama Barang</th>
                        <th width="120px">Kategori</th>
                        <th width="120px">Merek</th>
                        <th width="100px">Stok</th>
                        <th width="140px">Harga Satuan (Rp)</th>
                        <th width="200px">Keterangan</th>
                        <th>Batal</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <div class="text-right">
                <button type="button" id="newRow" class="btn btn-success btn-sm"><i class="fa fa-add"></i> Tambah Baris</button>
            </div>
        </div>
        <?php echo createBtnForm('Simpan') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    btnSubmit = $(':button[type="submit"]');
    brand     = <?php echo $brand ?>;
    category  = <?php echo $category ?>;

    $(document).ready(function(){
        trNew()
        deleteColoumn()
        $('.select2').change(function(){
            $(this).valid();
        })

        $('.rp').keyup(function(e){
            this.value = formatRupiah(this.value);
        })

        $('#newRow').click(function(){
            trNew()
            deleteColoumn()
            if(btnSubmit.attr('disabled')){
                btnSubmit.prop('disabled', false);
            }
        })

        $('#ff').validate({
            ignore      : 'input[type=hidden], .select2-search__field', 
            errorClass  : 'validation-error-label',
            successClass: 'validation-valid-label',
            rules       : rules,
            messages    : messages,

            highlight   : function(element, errorClass) {
                $(element).removeClass(errorClass);
            },

            unhighlight : function(element, errorClass) {
                $(element).removeClass(errorClass);
            },

            errorPlacement: function(error, element) {
                if (element.parents('div').hasClass('has-feedback')) {
                    error.appendTo( element.parent() );
                }

                else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo( element.parent() );
                }

                else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                $.ajax({
                    url         : form.action,
                    data        : $(form).serialize(),
                    type        : 'POST',
                    dataType    : 'json',

                    beforeSend: function(){
                        unBlockUiId('ff')
                    },

                    success: function(json) {
                        if(json.status == 'success'){
                            closeModal();
                            alertify.success(json.message);
                            $('#grid').datagrid('reload');
                        }else{
                            alertify.error(json.message);
                        }
                    },

                    error: function() {
                        alertify.error('Silahkan Hubungi Administrator');
                    },

                    complete: function(){
                        $('#ff').unblock(); 
                    }
                });
            }
        });        
    })

    $('.deleteHtml').click(function(e){
            e.preventDefault();
            $(this).parent().parent().remove();

            var i = 1;
            $('#addItem tbody tr').each(function(){
                console.log($(this).find('td:nth-child(1)'))
                $(this).find('td:nth-child(1)').html(i);
                i++;
            });

            a = i - 1;
            if(!a){
                btnSubmit.prop('disabled', true);
            }
        })

    function deleteColoumn(){
        $('.deleteHtml').click(function(e){
            e.preventDefault();
            $(this).parent().parent().remove();

            var i = 1;
            $('#addItem tbody tr').each(function(){
                code = $(this).find('td:nth-child(2)')[0].children;
                $(this).find('td:nth-child(1)').html(i);
                i++;
            });

            a = i - 1;
            if(!a){
                btnSubmit.prop('disabled', true);
            }
        })
    }

    function trNew(){
        var ar = $('#addItem tbody tr').length;
        var nomor = ar + 1;
        
        var html = "<tr>";
        html += "<td>"+nomor+"</td>";
        html += "<td><input type='text' name='code[]' class='form-control input-sm kode_barang' required></td>";
        html += "<td><input type='text' name='name[]' class='form-control input-sm' required></td>";
        html += "<td>";
        html += "<select name='category[]' class='form-control input-sm category select2' data-placeholder='Pilih' required>";
        html += "</select>";
        html += "</td>";
        html += "<td>";
        html += "<select name='brand[]' class='form-control input-sm brand select2' data-placeholder='Pilih' required>";
        html += "</select>";
        html += "</td>";
        html += "<td><input type='text' name='stock[]' class='form-control input-sm rp' required></td>";
        html += "<td><input type='text' name='price[]' class='form-control input-sm rp' required></td>";
        html += "<td><textarea name='note[]' class='form-control input-sm'  style='resize: none;'></textarea></td>";
        html += "<td align='center'><a href='#' class='deleteHtml'><i class='fa fa-times' style='color:red;'></i></a></td>";
        html += "</tr>";

        $('#addItem tbody').append(html);

        $('.category').select2({
            data: category
        })

        $('.brand').select2({
            data: brand
        })
    }
</script>