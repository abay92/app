<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * --------------------
 * CLASS NAME : Khutbah
 * --------------------
 *
 * @author     Robai <abayr92@gmail.com>
 * @copyright  2018
 *
 */

class Khutbah extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('khutbah_model');
        $this->module = 'khutbah';
        $this->_table = 't_mtr_khutbah';
        $this->_sess  = $this->session->userdata('app_session');
        $this->_code  = decode($this->_sess['code']);
    }
 	
 	function index() {
 		$data['title']   = "Khutbah Jum'at";
 		$data['content'] = "khutbah/index";
        $data['year']    = $this->khutbah_model->list_year($this->_code);
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');

        $this->load->view('template/page', $data);
    }

    function add(){
        validate_ajax();
        $data['title']  = "Tambah Khutbah Jum'at";
        $data['friday'] = date('d F Y', strtotime('next friday') );

        $this->load->view('khutbah/add',$data);
    }

    function edit($param=''){
        validate_ajax();
        $data['title']   = "Edit Khutbah Jum'at";
        $data['row']     = $this->mglobal->selectDataByID($this->_table, 'khutbah_id', decode($param));
        $data['id']      = $param;
        $data['date']    = date('d F Y', strtotime($data['row']->date));
        $this->load->view('khutbah/edit',$data);
    }

    function action_add(){
        validate_ajax();
        $post   = $this->input->post();

        /* validation */
        $this->form_validation
        ->set_rules('imam', 'Imam', 'trim|required')
        ->set_rules('khotib', 'Khotib', 'trim|required')
        ->set_rules('muadzin', 'Muadzin', 'trim|required')
        ->set_rules('date', 'Tanggal', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        $date = date('Y-m-d', strtotime($post['date']));
        /* data post */
        $data = array(
            'imam' => $post['imam'],
            'khotib' => $post['khotib'],
            'muadzin' => $post['muadzin'],
            'date' => $date,
            'company_code' => $this->_code
        );

        $where = array(
            'date' => $date,
            'company_code' => $this->_code,
        );

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($this->mglobal->checkData($this->_table, $where)){
            $response =  json_api('failed','Tanggal '.$post['date'].' Sudah Ada'); 
        }else{
            $save = $this->mglobal->saveData($this->_table, $data);
            if($save){
                $response = json_api('success','Simpan Data Berhasil');
            }else{
                $response = json_encode($this->db->error());
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;     
    }

    function action_edit(){
        validate_ajax();
        $post   = $this->input->post();

        /* validation */
        $this->form_validation
        ->set_rules('imam', 'Imam', 'trim|required')
        ->set_rules('khotib', 'Khotib', 'trim|required')
        ->set_rules('muadzin', 'Muadzin', 'trim|required')
        ->set_rules('date', 'Tanggal', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        $date = date('Y-m-d', strtotime($post['date']));
        $id   = decode($post['id']);

        /* data post */
        $data = array(
            'khutbah_id' => $id,
            'imam' => $post['imam'],
            'khotib' => $post['khotib'],
            'muadzin' => $post['muadzin'],
            'date' => $date,
            'company_code' => $this->_code
        );

        $where = array(
            'date' => $date,
            'company_code' => $this->_code
        );

        $checkDate = $this->mglobal->checkData($this->_table, $where, 'khutbah_id', $id);

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($checkDate){
            $response =  json_api('failed','Tanggal '.$post['date'].' Sudah Ada'); 
        }else{
            $update = $this->mglobal->updateData($this->_table, $data, 'khutbah_id');

            if($update){
                $response = json_api('success','Update Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;
    }

    function delete($param=''){
        validate_ajax();
        $delete = $this->mglobal->deleteDataById($this->_table,'khutbah_id',decode($param));
        if($delete){
            $response =  json_api('success','Delete Data Berhasil');
        }else{
            $response =  json_encode($this->db->error());
        }    

        $data = array(
            'khutbah_id'=> decode($param)
        );

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log); 

        echo $response;  
    }

    function get_list(){
    	validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->khutbah_model->get_list($this->_code);

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                $r->khutbah_id= encode($r->khutbah_id);
                $r->date      = date_indo($r->date);
                $r->action    = createBtnAction($this->module,'edit',$r->khutbah_id).' ';
                $r->action   .= createBtnAction($this->module,'delete',$r->khutbah_id);

                $rows_data[]= $r;
                unset($r->khutbah_id);
            }
        }

        $data['total'] = $dataList['total'];
        $data['rows']  = $rows_data;
        
        echo json_encode($data);
    }
}
