<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ------------------
 * CLASS NAME : Login
 * ------------------
 *
 * @author     Robai <abayr92@gmail.com>
 * @copyright  2018
 *
 */

date_default_timezone_set("Asia/Jakarta");
class Login extends MX_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->helper('captcha');
    }
 	
 	function index() {
        if($this->session->userdata('app_session')){
            redirect(site_url('home'));
        }

        $data['title']  = "Login";
        $data['action'] = site_url('login/action_login');
        $security       = $this->createCaptcha();
        $data['img']    = $security['image'];
        $this->session->set_userdata('captcha', $security['word']);

        $this->load->view("login/index",$data);
    }

    function createCaptcha(){
        $path = './captcha/';

        @chmod($path,0777);

        if(!file_exists($path)){
            mkdir($path, 0777, true);
        }
        $config = array(
            'word'      => strtolower(substr(md5(time()),0,5)),
            'img_path'  => $path,
            'img_url'   => base_url().'captcha/',
            'img_width' => 150,
            'img_height'=> 40,
            'border'    => 5, 
            'word_length'=> 5,
            'font_size' => 17,
            'font_path' => FCPATH.'assets/font/verdana.ttf',
            'expiration'=> 900,
            'colors'        => array(
                'background' => array(60, 141, 188),
                'border' => array(60, 141, 188),
                'text' => array(255, 255, 255),
                'grid' => array(60, 141, 188)
            )
        );

        return create_captcha($config);
    }

    function action_login(){
        validate_ajax();

        $post       = $this->input->post();
        $check_user = $this->mglobal->checkLogin('user_name',$post['username']);
        $captcha    = $this->session->userdata('captcha');
        // print_r($post);exit;
        //     echo $this->config->item('encryption_key') . $post['password'];exit;


        if($post['captcha'] != $captcha){
            echo json_api('failed','Captcha Salah');
        }else

        if(!$check_user){
            echo json_api('failed','Username belum terdaftar'); 
        }elseif(!password_verify($this->config->item('encryption_key') . $post['password'], $check_user->password)){
            echo json_api('failed','Password yang Anda Masukkan Salah'); 
        }elseif(!$check_user->active){
            echo json_api('failed','Akun Anda diblokir'); 
        }elseif($check_user->is_login != 0){
            if(date('Y-m-d', strtotime($check_user->last_login)) != date('Y-m-d')){
                $dataUpdate = array(
                    'user_id'   => $check_user->user_id, 
                    'is_login'  => 0,
                );
                $this->mglobal->updateData('t_mtr_user', $dataUpdate, 'user_id');
                echo json_api('failed','Tunggu 30 detik, setelah itu lakukan proses login kembali');
            }else{
                echo json_api('failed','User Sedang Login ditempat Lain');
            }
        }else{
            $dataUpdate = array(
                'user_id'   => $check_user->user_id, 
                'is_login'  => 1, 
                'last_login'=> date('Y-m-d H:i:s')
            );
            $update = $this->mglobal->updateData('t_mtr_user', $dataUpdate, 'user_id');

            if($update){
                $data = $this->mglobal->checkLogin('user_name',$post['username']);
                $session = array(
                    'user'      => encode($data->user_id), 
                    'signup'    => date("M Y", strtotime($data->created_on)),
                    'fullname'  => $data->full_name, 
                    'level_name'=> $this->mglobal->selectDataByID('t_mtr_user_group', 'group_id', $data->user_group)->group_name,
                    'level'     => $data->user_group, 
                    'code'      => encode($data->company_code), 
                    'is_login'  => $data->is_login, 
                    'change_password' => $data->must_change_password,
                    'last_login'=> $data->last_login,
                    'expired_login'=> date('Y-m-d 23:59:59'),
                    'menu'      => $this->mglobal->getMenu($data->user_group)
                );
                echo json_api('success',site_url('home'));
                $this->session->set_userdata('app_session', $session);
            }else{
                echo json_api('failed','Update Data Gagal'); 
            }
           
        }
    }

    function logout(){
        validate_ajax();
        $session = $this->session->userdata('app_session');
        if($session){
            $dataUpdate = array(
                'user_id'   => decode($session['user']), 
                'is_login'  => 0,
            );
            $this->mglobal->updateData('t_mtr_user', $dataUpdate, 'user_id');
        }
       
        $this->session->unset_userdata('app_session');
        echo json_api('success',site_url());
    }
}
