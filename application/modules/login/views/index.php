<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo config_item('name_web'); ?> - Login</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link href='<?php echo config_item('icon'); ?>favicon.png' type='image/x-icon' rel='shortcut icon'>

	<!-- css -->
	<link href="<?php echo config_item('bootstrap'); ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo config_item('font'); ?>font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo config_item('font'); ?>Ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="<?php echo config_item('css'); ?>AdminLTE.min.css" rel="stylesheet">
	<link href="<?php echo config_item('css'); ?>global.css" rel="stylesheet">
	<link href="<?php echo config_item('plugins'); ?>iCheck/square/blue.css" rel="stylesheet">
	<link href="<?php echo config_item('plugins'); ?>alertify/css/alertify.core.css" rel="stylesheet">
	<link href="<?php echo config_item('plugins'); ?>alertify/css/alertify.default.css" rel="stylesheet">

	<!-- js -->
	<script src="<?php echo config_item('js'); ?>jquery/jquery.min.js"></script>
	<script src="<?php echo config_item('bootstrap'); ?>js/bootstrap.min.js"></script>
	<script src="<?php echo config_item('plugins'); ?>iCheck/icheck.min.js"></script>
	<script src="<?php echo config_item('js'); ?>validate.min.js"></script>
	<script src="<?php echo config_item('js'); ?>global.js"></script>
	<script src="<?php echo config_item('js'); ?>md5.js"></script>
	<script src="<?php echo config_item('plugins'); ?>alertify/js/alertify.min.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->
</head>
<style type="text/css">
	html, body {
	    height: 90%;
	}
	.login-page, .register-page {
	    background: #204d74;
	}
</style>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="<?php echo site_url('home') ?>"><p style="color: white"><?php echo config_item('title_web'); ?></p></a>
		</div>
		<div class="login-box-body">
			<p class="login-box-msg">Login</p>

			<form id="fLogin" action="<?php echo $action; ?>" method="post">
				<div class="form-group has-feedback">
					<input type="text" class="form-control" placeholder="Username" name="username" id="username" autocomplete="off" required>
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" placeholder="Password" name="password" id="password" autocomplete="off" required>
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<center>
						<div id="img_captcha">
							<p><b><?php echo $img; ?></b></p>
						</div>
					</center>
				</div>
				<div class="form-group has-feedback">
					<input type="text" class="form-control" placeholder="Captcha" name="captcha" autocomplete="off" required>
					<span class="glyphicon glyphicon-exclamation-sign form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-7">
			          <div class="checkbox icheck">
			            <label>
			              <input type="checkbox" id="showPass" onclick="showPassword()"> Lihat Password
			            </label>
			          </div>
			        </div>
					<div class="col-xs-5">
						<button id="btnLogin" type="submit" class="btn btn-primary btn-block btn-flat" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Memeriksa...">Login</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script>
		$(function () {
			$('#username').focus()
			$('#showPass').iCheck({
		      	checkboxClass: 'icheckbox_square-blue',
		      	radioClass: 'iradio_square-blue',
		      	increaseArea: '20%'
		    });

		    $("#showPass").on("ifChanged", showPassword);
		    actionLogin()
		});
	</script>
</body>
</html>
