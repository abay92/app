<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * -----------------
 * CLASS NAME : Menu
 * -----------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Menu extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('menu_model');
        $this->module = 'menu';
        $this->_table = 't_mtr_menu';
    }
 	
 	function index() {
 		$data['title']   = "Menu";
 		$data['content'] = "menu/index";
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');

        $this->load->view('template/page', $data);
    }

    function add(){
        validate_ajax();
        $data['title']  = 'Tambah Menu';
        $data['icon']   = $this->get_icons();
        $data['action'] = $this->get_actions();

        $this->load->view('menu/add',$data);
    }

    function edit($param=''){
        validate_ajax();
        $where       = array('menu_id' => decode($param));
        $menu_detail = $this->mglobal->selectData('t_mtr_menu_detail', $where);

        $arr_detail = array();
        foreach ($menu_detail as $val) {
            $arr_detail[] = $val->action_id;
        }

        $data['title']     = 'Edit Menu';
        $data['row']       = $this->mglobal->selectDataById($this->_table, 'menu_id', decode($param));
        $data['id']        = $param;
        $data['icon']      = $this->get_icons();
        $data['action']    = $this->get_actions();
        $data['menuaction']= json_encode($arr_detail);

        $this->load->view('menu/edit',$data);
    }

    function action_add(){
        validate_ajax();
        $post = $this->input->post();

        /* validation */
        $this->form_validation
            ->set_rules('name', 'Nama Menu', 'trim|required')
            ->set_rules('icon', 'Icon Menu', 'trim|required')
            ->set_rules('ordering', 'Urutan', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* check post parent id */
        $parent = $post['parentid'];
        $url    = ($post['url'] == '') ? '#' : $post['url'];
        if($post['parentid'] == ''){
            $parent = 0;
        }

        /* data post */
        $data = array(
            'name' => $post['name'], 
            'icon' => $post['icon'],
            'ordering' => $post['ordering'],
            'url' => $url,
            'parent_id' => $parent,
        );

        /* check url */
        $checkMenuUrl = false;
        if($url != '#'){
            $where = array(
                'LOWER(url)' => strtolower($post['url']),
            );
           
            $checkMenuUrl = $this->mglobal->checkData($this->_table, $where);        
        }

        /* check order */
        $where2 = array(
            'parent_id' => $parent,
            'ordering'  => $post['ordering'],
        );
        $checkOrder = $this->mglobal->checkData($this->_table, $where2);

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($checkMenuUrl){
            $response = json_api('failed','Url Sudah digunakan'); 
        }elseif($checkOrder){
            $response = json_api('failed','Urutan Sudah digunakan'); 
        }else{
            $save = $this->mglobal->saveData($this->_table, $data);
            if($save){
                $menu_detail = array();
                foreach ($post['menuAction'] as $val) {
                    $menu_detail[] = array(
                        'menu_id' => (int)$save, 
                        'action_id' => $val, 
                        'created_by' => decode($this->_sess['user']),
                        'created_on' => date('Y-m-d H:i:s'),
                    );
                }
                $this->db->insert_batch('t_mtr_menu_detail', $menu_detail);
                $response = json_api('success','Simpan Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }   

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;    
    }

    function action_edit(){
        validate_ajax();
        $post   = $this->input->post();
        $id     = decode($post['id']);

        /* validation */
        $this->form_validation
            ->set_rules('name', 'Nama Menu', 'trim|required')
            ->set_rules('icon', 'Icon Menu', 'trim|required')
            ->set_rules('ordering', 'Urutan', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* check post parent id */
        $parent = $post['parentid'];
        $url    = ($post['url'] == '') ? '#' : $post['url'];

        if($post['parentid'] == ''){
            $parent = 0;
        }

        /* data post */
        $data = array(
            'menu_id'   => $id, 
            'name'      => $post['name'], 
            'icon'      => $post['icon'],
            'ordering'  => $post['ordering'],
            'url'       => $url,
            'parent_id' => $parent,
        );

        /* check url */
        $checkMenuUrl = false;
        if($url != '#'){
            $where = array(
                'LOWER(url)'    => strtolower($url),
            );
            $checkMenuUrl = $this->mglobal->checkData($this->_table, $where, 'menu_id', $id);      
        }

        /* check order */
        $where2 = array(
            'parent_id' => $parent,
            'ordering'  => $post['ordering'],
        );
        $checkOrder = $this->mglobal->checkData($this->_table, $where2, 'menu_id', $id);

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($checkMenuUrl){
            $response = json_api('failed','Url Sudah digunakan'); 
        }elseif($checkOrder){
            $response = json_api('failed','Urutan Sudah digunakan'); 
        }else{
            $update = $this->mglobal->updateData($this->_table, $data, 'menu_id');
            if($update){
                $list_detail= $this->menu_model->select_menu_detail($id);
                $arr_diff   = array_diff($list_detail, $post['menuAction']);
                if($arr_diff){
                    $detail_id = array();

                    foreach ($arr_diff as $key => $val) {
                        $detail_id[] = $key;
                    }
                    $this->mglobal->deleteDataMultiple('t_mtr_menu_detail','detail_id',$detail_id);
                    if($this->menu_model->select_menu_privilege($id,$detail_id)){
                        $this->menu_model->deletePrivilege($id,$detail_id);
                    }
                }

                $arr_diff2 = array_diff($post['menuAction'],$list_detail);

                if($arr_diff2){
                    $menu_detail = array();
                    foreach ($arr_diff2 as $val) {
                        $menu_detail[] = array(
                            'menu_id' => $id, 
                            'action_id' => $val, 
                            'created_by' => decode($this->_sess['user']),
                            'created_on' => date('Y-m-d H:i:s'),
                        );
                    }
                    $this->db->insert_batch('t_mtr_menu_detail', $menu_detail);
                }
                $response = json_api('success','Update Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }    

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;
    }

    function delete($param=''){
        validate_ajax();
        $id = decode($param);
        $delete = $this->mglobal->deleteDataById($this->_table,'menu_id',$id);
        if($delete){
            $detail = $this->mglobal->checkData('t_mtr_menu_detail', array('menu_id'=>$id));
            $privilege = $this->mglobal->checkData('t_mtr_privilege', array('menu_id'=>$id));

            if($detail){
                $this->mglobal->deleteDataMultiple('t_mtr_menu_detail','menu_id',$id);
            }

            if($privilege){
                $this->mglobal->deleteDataMultiple('t_mtr_privilege','menu_id',$id);
            }
            $response = json_api('success','Delete Data Berhasil');
        }else{
            $response = json_encode($this->db->error()); 
        }  

        $data = array(
            'menu_id'=> decode($param)
        );

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;     
    }

    function get_list(){
    	validate_ajax();
        echo json_encode($this->menu_model->get_list());
    }

    function get_actions(){
        $data_group = $this->mglobal->selectAll('t_mtr_action');
        $data = array();
        foreach($data_group as $row){
          $data[$row->action_id] = $row->action_name;
        }
        return $data;
    }

    function get_icons(){
        $icon = $this->mglobal->selectAll('t_mtr_icon');
        $data = array();
        foreach($icon as $row){
          $data["{$row->icon_name}"] = $row->icon_name;
        }
        return $data;
    } 
}
