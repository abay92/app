<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * -----------------------
 * CLASS NAME : Menu_model
 * -----------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Menu_model extends CI_Model{

	function __construct() {
        parent::__construct();
        $this->module = 'menu';
    }

	function get_list($pi=0) {
		$result = array();
		$items  = array();

		$sql 	= "SELECT menu_id, name, icon, url, parent_id, ordering, active 
				   FROM t_mtr_menu 
				   WHERE parent_id = $pi AND active = 1 
				   ORDER BY ordering ASC";
		$query 	= $this->db->query($sql)->result();

		$active    = '<span class="label bg-green">Active</span>';
        $nonactive = '<span class="label bg-red">Not Active</span>';
		
		if($query){
			foreach ($query as $row){
				$has_child 	  = $this->check_parent($row->menu_id);
				$row->iconCls = $row->icon;
				$row->state	  = 'open';
	            $row->active  = $row->active ? $active : $nonactive;

	            if($has_child){
					$row->children = $this->get_list_children($row->menu_id);
				}

	            $row->action  = createBtnAction($this->module,'edit',encode($row->menu_id)).' ';

	            if(strtolower($row->name) != 'menu' && strtolower($row->name) != 'privilege'){
	            	$row->action  .= createBtnAction($this->module,'delete',encode($row->menu_id)).' ';
	            }
					
				array_push($items, $row);
			}
		}
		
		$result["rows"] = $items;
		
		return $result;
	}

	function get_list_children($pi) {
		$items  = array();

		$sql 	= "SELECT menu_id, name, icon, url, parent_id, ordering, active 
				   FROM t_mtr_menu 
				   WHERE parent_id = $pi AND active = 1 
				   ORDER BY ordering ASC";
		$query 	= $this->db->query($sql)->result();

		$active    = '<span class="label bg-green">Active</span>';
        $nonactive = '<span class="label bg-red">Not Active</span>';
		
		if($query){
			foreach ($query as $row){
				$has_child 	  = $this->check_parent($row->menu_id);
				$row->iconCls = $row->icon;
				$row->state	  = 'open';
	            $row->active  = $row->active ? $active : $nonactive;

	            if($has_child){
					$row->children = $this->get_list_children($row->menu_id);
				}

	            $row->action  = createBtnAction($this->module,'edit',encode($row->menu_id)).' ';

	            if(strtolower($row->name) != 'menu' && strtolower($row->name) != 'privilege'){
	            	$row->action  .= createBtnAction($this->module,'delete',encode($row->menu_id)).' ';
	            }
					
				array_push($items, $row);
			}
		}
		
		return $items;
	}

	function check_parent($id){
		$sql   = "SELECT * FROM t_mtr_menu WHERE parent_id = $id";
		$query = $this->db->query($sql);
		$row   = $query->num_rows();

		return $row > 0 ? true : false;
	}

	function select_menu_detail($menuid){
        $data= array();
		$sql = "SELECT detail_id, action_id
				FROM t_mtr_menu_detail 
				WHERE active = 1 AND menu_id = $menuid 
				ORDER BY action_id ASC";

		$result = $this->db->query($sql)->result();

		if($result){
            foreach ($result as $row) {
                $data[$row->detail_id] = $row->action_id;
            }
        }

        return $data;
	}

	function select_menu_privilege($menuid,$menudetail){
		$this->db->where('menu_id', $menuid);
		$this->db->where_in('menu_detail_id', $menudetail);
		return $this->db->get('t_mtr_privilege')->result();
	}

	function deletePrivilege($menuid,$menudetail){
		$this->db->where('menu_id', $menuid);
		$this->db->where_in('menu_detail_id', $menudetail);
		$this->db->delete('t_mtr_privilege');
	    return $this->db->affected_rows();
	}
}