<?php 

/**
 * ------------------------------
 * NAME FILE : add.php - form add
 * ------------------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

?>

<div class="col-md-8 col-md-offset-2">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h1 class="box-title"><?php echo $title; ?></h1>
        </div>
        <?php echo form_open('menu/action_add', 'id="ff" autocomplete="off" '); ?>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4">
                        <label>Nama Menu</label>
                        <input type="text" name="name" class="form-control" placeholder="Nama Menu" required>
                    </div>
                    <div class="col-sm-8">
                        <label>URL</label>
                        <div class="input-group group">
                            <span class="input-group-addon"><?php echo site_url(); ?></span>
                            <input type="text" name="url" class="form-control in-group" placeholder="URL">
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Urutan</label>
                        <input type="number" min="1" name="ordering" class="form-control" placeholder="Urutan" required>
                    </div>
                    <div class="col-sm-3">
                        <label>Icon</label>
                        <?php echo form_dropdown('icon', $icon, '', 'class="form-control select2" id="icon" required'); ?>
                    </div>
                    <div class="col-sm-6">
                        <label>Parent</label>
                        <input type="text" class="form-control" name="parentid" id="parentid" style="width: 100%">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Aksi</label>
                        <?php echo form_dropdown('menuAction', $action, '', 'class="form-control select2" id="menuAction" required data-placeholder="Pilih Aksi" multiple="multiple"'); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Simpan') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.mfp-wrap').removeAttr('tabindex')
        $('.select2').change(function(){
            $(this).valid()
        })

        $("#menuAction").select2()

        $("#icon").select2({
            templateResult: iconFormat,
            templateSelection: iconFormat,
            escapeMarkup: function(m) { return m; }
        });

        $('#parentid').combotreegrid({
            url         : baseURL+'menu/get_list',
            method      : 'POST',
            striped     : false,
            fitColumns  : true,
            treeField   : 'name',
            idField     : 'menu_id',
            height      : 'auto',
            selectOnNavigation: false,
            scrollbarSize: 0,
            nowrap      : true,
            sortable    : false,
            singleSelect: true,
            columns:[[
              { field: 'name', title: 'Menu Name', width: 100}
            ]],

            loadFilter:function(data){
                data.rows.push(
                    {menu_id: 0, name: "No Parent"}
                );
                
                return data;
            },

            // onLoadSuccess: function(row, data){
            //     $('.tree-folder').css('background','none');
            //     $('.tree-file').css('background','none');

            //     r = data.rows;
            //     for(i in r){
            //         $('.'+r[i].iconCls).html('<i class="fa fa-'+r[i].iconCls+'"></i>');
            //     }

            //     $('.circle-o').html('<i class="fa fa-circle-o"></i>');
            // }
        });

        validateForm('#ff',function(url,data){
            data.menuAction = $('#menuAction').val();
            $.ajax({
                url         : url,
                data        : data,
                type        : 'POST',
                dataType    : 'json',

                beforeSend: function(){
                    unBlockUiId('ff')
                },

                success: function(json) {
                    if(json.status == 'success'){
                        closeModal();
                        alertify.success(json.message);
                        $('#grid').treegrid('reload');
                    }else{
                        alertify.error(json.message);
                    }
                },

                error: function() {
                    alertify.error('Silahkan Hubungi Administrator');
                },

                complete: function(){
                    $('#ff').unblock(); 
                }
            });
        });
    })
</script>