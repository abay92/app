<?php 

/**
 * --------------------------------
 * NAME FILE : edit.php - form edit
 * --------------------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

?>

<div class="col-md-8 col-md-offset-2">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('menu/action_edit', 'id="ff" autocomplete="off" '); ?>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4">
                        <label>Nama Menu</label>
                        <input type="text" name="name" class="form-control" placeholder="Nama Menu" required value="<?php echo $row->name ?>">
                        <input type="hidden" name="id" required value="<?php echo $id ?>">
                    </div>
                    <div class="col-sm-8">
                        <label>URL</label>
                        <div class="input-group">
                            <span class="input-group-addon"><?php echo site_url(); ?></span>
                            <input type="text" name="url" class="form-control in-group" placeholder="URL" required value="<?php echo $row->url ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Icon</label>
                        <?php echo form_dropdown('icon', $icon, $row->icon, 'class="form-control select2" id="icon" required'); ?>
                    </div>
                    <div class="col-sm-2">
                        <label>Urutan</label>
                        <input type="number" min="1" name="ordering" class="form-control" placeholder="Urutan" required value="<?php echo $row->ordering ?>">
                    </div>
                    <div class="col-sm-7">
                        <label>Parent</label>
                        <input type="text" class="form-control" name="parentid" id="parentid" style="width: 100%;">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Aksi</label>
                        <?php echo form_dropdown('menuAction', $action, '', 'class="form-control select2" id="menuAction" required data-placeholder="Pilih Aksi" multiple="multiple"'); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Update') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.mfp-wrap').removeAttr('tabindex')
        $('.select2').change(function(){
            $(this).valid()
        })

        $('#menuAction').select2()

        $('#icon').select2({
            templateResult: iconFormat,
            templateSelection: iconFormat,
            escapeMarkup: function(m) { return m; }
        });

        setTimeout(function(){
            $('#menuAction').val(<?php echo $menuaction; ?>);
            $('#menuAction').trigger('change');
        },200)

        $('#parentid').combotreegrid({
            url         : baseURL+'menu/get_list',
            method      : 'POST',
            striped     : false,
            fitColumns  : true,
            treeField   : 'name',
            idField     : 'menu_id',
            height      : 'auto',
            selectOnNavigation: false,
            scrollbarSize: 0,
            nowrap      : true,
            sortable    : false,
            singleSelect: true,
            columns:[[
              { field: 'name', title: 'Menu Name', width: 100}
            ]],

            loadFilter:function(data){
                data.rows.push(
                    {menu_id: 0, name: "No Parent"}
                );
                
                return data;
            },

            // onLoadSuccess: function(row, data){
            //     $('.tree-folder').css('background','none');
            //     $('.tree-file').css('background','none');

            //     r = data.rows;
            //     for(i in r){
            //         $('.'+r[i].iconCls).html('<i class="fa fa-'+r[i].iconCls+'"></i>');
            //     }

            //     $('.th').html('<i class="fa fa-th"></i>');
            //     $('#parentid').combotreegrid('setValue', <?php echo $row->parent_id; ?>);
            // }
        });

        validateForm('#ff',function(url,data){
            data.menuAction = $('#menuAction').val();
            $.ajax({
                url         : url,
                data        : data,
                type        : 'POST',
                dataType    : 'json',

                beforeSend: function(){
                    unBlockUiId('ff')
                },

                success: function(json) {
                    if(json.status == 'success'){
                        closeModal();
                        alertify.success(json.message);
                        $('#grid').treegrid('reload');
                    }else{
                        alertify.error(json.message);
                    }
                },

                error: function() {
                    alertify.error('Silahkan Hubungi Administrator');
                },

                complete: function(){
                    $('#ff').unblock(); 
                }
            });
        });
    })
</script>