<?php 

/**
 * ----------------------------
 * NAME FILE : index.php - list
 * ----------------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

?>

<div class="content-wrapper">
	<section class="content">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $title ?></h3>
                <div class="box-tools pull-right">
                    <?php echo $btn_add ?>
                </div>
            </div>

            <div class="box-body no-padding">
                <table class="table" id="grid"></table>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#grid').treegrid({
            url         : baseURL+'menu/get_list',
            method      : 'POST',
            striped     : false,
            fitColumns  : true,
            treeField   : 'name',
            idField     : 'menu_id',
            maxHeight   : '500px',
            emptyMsg    : 'Tidak Ada Data.',
            loadMsg     : 'Memproses, tunggu sebentar.',
            scrollbarSize: 0,
            nowrap      : true,
            sortable    : false,
            singleSelect: true,
            columns:[[
                { field: 'name', title: 'Nama Menu', width: 50},
                { field: 'ordering', title: 'Urutan', width: 20,align: 'center'},
                { field: 'active', title: 'Status', width: 20,align: 'center'},
                { field: 'action', title: 'Aksi', width: 20,align: 'center'},
            ]],

            onLoadSuccess: function(row, data){
                // $('.tree-folder').css('background','none');
                // $('.tree-file').css('background','none');

                $('.sidebar-toggle').click(function(){
                    setTimeout(function(){
                        $('#grid').treegrid('resize');
                    },400)
                });

                $(window).resize(function(){
                    setTimeout(function(){
                        $('#grid').treegrid('resize');
                    },400)
                })

                // r = data.rows;
                // for(i in r){
                //     $('.'+r[i].iconCls).html('<i class="fa fa-'+r[i].iconCls+'"></i>');
                // }

                // $('.circle-o').html('<i class="fa fa-circle-o"></i>');
            }
        });
    });

    function confirmationAction(message,url){
        alertify.confirm(message, function (e) {
            if(e){
                $.ajax({
                url         : url,
                type        : 'GET',
                dataType    : 'json',

                beforeSend: function(){
                    blockUi()
                },

                success: function(json) {
                    if(json.status == 'success'){
                        alertify.success(json.message);
                        $('#grid').treegrid('reload');
                    }else{
                        alertify.error(json.message);
                    }
                },

                error: function() {
                    alertify.error('Silahkan Hubungi Administrator');
                },

                complete: function(){
                    $.unblockUI();
                }
            });
            }
        });
    }
</script>