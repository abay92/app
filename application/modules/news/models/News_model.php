<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * -----------------------
 * CLASS NAME : News_model
 * -----------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class News_model extends CI_Model {
	function get_list() {
		$data 	 = array();
		$search  = trim(strtoupper($this->db->escape_like_str($this->input->post('search'))));
		$page 	 = $this->input->post('page') ? $this->input->post('page') : 1;
		$rows 	 = $this->input->post('rows') ? $this->input->post('rows') : 10;
		$offset  = ($page - 1) * $rows;
		$sort 	 = $this->input->post('sort') ? $this->input->post('sort') : 'news_id';
		$order 	 = $this->input->post('order') ? strtoupper($this->input->post('order')) : 'DESC';

		$where 	 = "WHERE n.active = 1";		
		
		if($search != ''){
			$where .= " AND (UPPER(title) LIKE '%{$search}%' OR UPPER(category_name) LIKE '%{$search}%')";
		}

		$sql = "SELECT news_id, title, category_name, content, tag FROM t_mtr_news n
				LEFT JOIN t_mtr_news_category c ON c.category_id = n.category_id AND c.active = 1
				{$where} ORDER BY {$sort} {$order}";
		
		$query 		= $this->db->query($sql);
		$total_rows = $query->num_rows();
		$sql 	   .= " LIMIT {$rows} OFFSET {$offset}";
		$query2		= $this->db->query($sql);
				
		$data['total'] = $total_rows;
		$data['rows']  = $query2->result();
		
		return $data;
	}

	function get_news_tag($id) {
		$sql = "SELECT tag_name FROM t_mtr_tag_category WHERE tag_id IN ($id) AND active = 1 ";
		$query = $this->db->query($sql);		
		return $query->result();
	}

	function download() {
		$sql = "SELECT item_code, item_name, category_name, brand_name, stock, price, note
				FROM t_mtr_item i
				LEFT JOIN t_mtr_item_category c ON c.category_code = i.category_code AND c.active = 1
				LEFT JOIN t_mtr_item_brand b ON b.brand_code = i.brand_code AND b.active = 1
				WHERE i.active = 1";
		$query = $this->db->query($sql);		
		return $query->result();
	}
}