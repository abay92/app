<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('item/category/action_add', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4">
                        <label>Kode Kategori</label>
                        <input type="text" name="code" class="form-control focus" placeholder="Kode Merk" required>
                    </div>
                    <div class="col-sm-8">
                        <label>Nama Kategori</label>
                        <input type="text" name="name" class="form-control" placeholder="Nama Merk" required>
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Simpan') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        validateForm('#ff',function(url,data){
           postData(url,data);
        });
    })
</script>