<div class="content-wrapper">
	<section class="content">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $title ?></h3>
                <div class="box-tools pull-right">
                    <?php echo $btn_add ?>
                </div>
            </div>
            <div id="tb">
                <div class="col-md-3 col-md-offset-9 pad-top">
                    <div class="form-group mar-bottom">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Cari nama kategori" id="search" autocomplete="off">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-flat" id="btnSearch">Cari</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body no-padding">
                <table class="table" id="grid"></table>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        settingDefaultDatagrid()
        $('#grid').datagrid({
            url        : baseURL+'item/category/get_list',
            columns : [[
                { field: 'category_code', title: 'Kode Kategori', width: 50, sortable  : true},
                { field: 'category_name', title: 'Nama Kategori', width: 100, sortable  : true},
                { field: 'action', title: 'Aksi', width: 100, align: 'center'}
            ]],

            onLoadSuccess: onLoadSuccessDatagrid,
        })

        $('#btnSearch').click(function(){
            searchingDatagrid()
        })

        $('#search').keydown(function(e) {
            if (e.which == 13){
                searchingDatagrid()
            }
        })
    })
</script>