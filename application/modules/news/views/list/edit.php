<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('item/item_list/action_edit', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Kode Barang</label>
                        <input type="text" name="code" class="form-control focus" placeholder="Kode Barang" required value="<?php echo $row->item_code ?>">
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                    </div>
                    <div class="col-sm-6">
                        <label>Nama Barang</label>
                        <input type="text" name="name" class="form-control" placeholder="Nama Barang" required value="<?php echo $row->item_name ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Kategory</label>
                        <?php echo form_dropdown('category', $category, encode($row->category_code), 'class="form-control select2" required data-placeholder="Pilih Kategori"'); ?>
                    </div>
                    <div class="col-sm-6">
                        <label>Merk</label>
                        <?php echo form_dropdown('brand', $brand, encode($row->brand_code), 'class="form-control select2" required data-placeholder="Pilih Merk"'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Stok</label>
                        <input type="text" name="stock" class="form-control rp" placeholder="Kode Barang" required value="<?php echo $stock ?>">
                    </div>
                    <div class="col-sm-6">
                        <label>Harga Satuan</label>
                        <input type="text" name="price" class="form-control rp" placeholder="Nama Barang" required value="<?php echo $price ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Keterangan</label>
                        <textarea name="note" class="form-control" rows="5" placeholder="Keterangan" style="resize: none;"><?php echo $row->note ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Update') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.mfp-wrap').removeAttr('tabindex')
        $('.select2').select2()
        $('.rp').keyup(function(e){
            this.value = formatRupiah(this.value);
        })

        validateForm('#ff',function(url,data){
           postData(url,data);
        });
    })
</script>