<div class="content-wrapper">
	<section class="content">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $title ?></h3>
                <div class="box-tools pull-right">
                    <?php echo $btn_add ?>
                </div>
            </div>
            <div id="tb">
                <div class="col-md-4 pad-top">
                    <div class="form-group mar-bottom">
                        <?php echo $btn_upload ?>
                        <?php echo $btn_download ?>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-5 pad-top">
                    <div class="form-group mar-bottom">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Cari..." id="search" autocomplete="off">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-flat" id="btnSearch">Cari</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body no-padding">
                <table class="table" id="grid"></table>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">   
    $(document).ready(function() {
        settingDefaultDatagrid()
        $('#grid').datagrid({
            url        : baseURL+'news/news_list/get_list',
            columns : [[
                { field: 'title', title: 'Judul', width: 100, sortable : true},
                { field: 'content', title: 'Konten', width: 100, sortable : true},
                { field: 'category_name', title: 'Kategori', width: 100, sortable : true},
                { field: 'tag', title: 'Tag', width: 100, sortable : true},
                { field: 'action', title: 'Aksi', width: 100, align: 'center'}
            ]],

            onBeforeLoad: function(param){
                $('#download').prop('disabled', true);

                $('#download').click(function(){
                    download(baseURL+'item/item_list/download',param);
                })
            },

            onLoadSuccess: function(row){
                if(row.total){
                    $('#download').prop('disabled', false);
                }
                $('#grid').datagrid('resize');
                $('.sidebar-toggle').click(function(){
                    setTimeout(function(){
                        $('#grid').datagrid('resize');
                    },400)
                });

                $(window).resize(function(){
                    setTimeout(function(){
                        $('#grid').datagrid('resize');
                    },400)
                })
            },
        })

        $('#btnSearch').click(function(){
            searchingDatagrid()
        })

        $('#search').keydown(function(e) {
            if (e.which == 13){
                searchingDatagrid()
            }
        })
    })
</script>