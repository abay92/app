<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ----------------------
 * CLASS NAME : Privilege
 * ----------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Privilege extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('privilege_model');
        $this->module = 'privilege';
        $this->_table = 't_mtr_privilege';
    }
 	
 	function index() {
 		$data['title']     = "Hak Akses Menu";
 		$data['content']   = "privilege/index";
        $data['usergroup'] = $this->list_user_group();

        $this->load->view('template/page', $data);
    }

    function get_list(){
    	validate_ajax();
        echo json_encode($this->privilege_model->get_list());
    }

    function set_privilege(){
        validate_ajax();
        $post = $this->input->post('params');
        $insert = array();
        $update = array();

        foreach ($post as $key => $value) {
            if($value['privilege'] == 0){
                $item['user_group_id']  = $value['group'];
                $item['menu_id']        = $value['menu_id'];
                $item['menu_detail_id'] = $value['detail_id'];
                $item['active']         = $value['status'];

                $insert[] = $item;
            }else{
                $item2['privilege_id']   = $value['privilege'];
                $item2['user_group_id']  = $value['group'];
                $item2['menu_id']        = $value['menu_id'];
                $item2['menu_detail_id'] = $value['detail_id'];
                $item2['active']         = $value['status'];

                $update[] = $item2;
            }
        }

        if($insert){
            $this->mglobal->insertBatch('t_mtr_privilege', $insert);
        }

        if($update){
            $this->mglobal->updateBatch('t_mtr_privilege', $update, 'privilege_id');
        }

        $response = json_api('success','Set privilege menu berhasil');

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode(array($insert,$update)),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;
    }

    function list_user_group(){
        $datas      = $this->mglobal->selectAll('t_mtr_user_group');
        $data['']   = '';

        if($datas){
            foreach($datas as $row){
                $data[$row->group_id] = $row->group_name;
            }
        }
        
        return $data;
    }
}
