<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ----------------------------
 * CLASS NAME : Privilege_model
 * ----------------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Privilege_model extends CI_Model{

	function __construct() {
        parent::__construct();
        $this->module = 'privilege';
    }

	function get_list($pi=0) {
		$result = array();
		$items  = array();
		$group  = $this->input->post('group');

		$sql 	= "SELECT menu_id, name, icon, url, parent_id, ordering, active 
				   FROM t_mtr_menu 
				   WHERE parent_id = $pi AND active = 1 
				   ORDER BY ordering ASC";
		$query 	= $this->db->query($sql)->result();

		$active    = '<span class="label bg-green">Active</span>';
        $nonactive = '<span class="label bg-red">Not Active</span>';
		
		if($query){
			foreach ($query as $row){
				$has_child 	  = $this->check_parent($row->menu_id);
				$row->iconCls = $row->icon;
				$row->state	  = 'open';
	            $row->active  = $row->active ? $active : $nonactive;

	            if($has_child){
					$row->children = $this->get_list_children($row->menu_id);
				}

				$parent = $row->parent_id;
		        if($row->parent_id == '' || $row->parent_id = null){
		            $parent = 0;
		        }

	            $row->action  = $this->select_menu_detail($row->menu_id,$parent,$group);
					
				array_push($items, $row);
			}
		}
		
		$result["rows"] = $items;
		
		return $result;
	}

	function get_list_children($pi) {
		$items  = array();
		$group  = $this->input->post('group');

		$sql 	= "SELECT menu_id, name, icon, url, parent_id, ordering, active 
				   FROM t_mtr_menu 
				   WHERE parent_id = $pi AND active = 1 
				   ORDER BY ordering ASC";
		$query 	= $this->db->query($sql)->result();

		$active    = '<span class="label bg-green">Active</span>';
        $nonactive = '<span class="label bg-red">Not Active</span>';
		
		if($query){
			foreach ($query as $row){
				$has_child 	  = $this->check_parent($row->menu_id);
				$row->iconCls = $row->icon;
				$row->state	  = 'open';
	            $row->active  = $row->active ? $active : $nonactive;

	            if($has_child){
					$row->children = $this->get_list_children($row->menu_id);
				}

				$parent = $row->parent_id;
		        if($row->parent_id == '' || $row->parent_id = null){
		            $parent = 0;
		        }

	            $row->action  = $this->select_menu_detail($row->menu_id,$parent,$group);
					
				array_push($items, $row);
			}
		}
		
		return $items;
	}

	function check_parent($id){
		$sql   = "SELECT * FROM t_mtr_menu WHERE parent_id = $id";
		$query = $this->db->query($sql);
		$row   = $query->num_rows();

		return $row > 0 ? true : false;
	}

	function select_menu_detail($menuid,$pi,$group){
        $data= array();
		$sql = "SELECT bb.name, bb.menu_id, aa.detail_id, dd.action_id,  dd.action_name, cc.privilege_id, cc.user_group_id, cc.active AS status
				FROM t_mtr_menu_detail aa
				LEFT JOIN t_mtr_menu bb ON aa.menu_id = bb.menu_id AND bb.active = 1 AND bb.menu_id = $menuid
				LEFT JOIN t_mtr_privilege cc ON aa.detail_id = cc.menu_detail_id AND cc.user_group_id = $group
				LEFT JOIN t_mtr_action dd ON dd.action_id = aa.action_id
				WHERE aa.active = 1 AND bb.parent_id = $pi
				ORDER BY action_id ASC";

		$html = "<div class='form-group' style='margin: auto;'>";
		$result = $this->db->query($sql)->result();

		if($result){
            foreach ($result as $row) {
            	$checked   = '';
            	$privilege = $row->privilege_id;
            	if($row->privilege_id == null || $row->privilege_id == ''){
            		$privilege  = 0;
            	}

            	if($row->status == 1){
            		$checked  = 'checked';
            	}

            	$html .= "<label><input type='checkbox' data-menu_id='{$row->menu_id}' data-detail_id='{$row->detail_id}' data-privilege='{$privilege}' class='act' {$checked}> {$row->action_name}</label> ";
            	$data[] = $html;
            }
        }
        
        $html .= "</div>";

        return $html;
	}
}