<div class="content-wrapper">
	<section class="content">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $title ?></h3>
            </div>
            <div id="tb">
                <div style="padding: 5px">
                    <div class="row">
                        <div class="col-md-4">
                            <?php echo form_dropdown('usergroup', $usergroup, '', 'class="form-control select2" id="group" required data-placeholder="Pilih User Grup"'); ?>
                        </div>
                        <div class="col-md-2">
                            <label><input type="checkbox" id="checkAll" disabled /> Check All</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body no-padding">
                <table class="table" id="grid"></table>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#group').select2();
        $('#group').on('select2:selecting', function(e) {
            val = e.params.args.data.id;
            gridPrivilege(val)
            $('#checkAll').iCheck('enable');
            $('#checkAll').iCheck('uncheck');
        });

        $('#checkAll').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass   : 'iradio_flat-blue'
        })

        .on('ifClicked', function (e) {
            if(e.currentTarget.checked){
                $('.act').iCheck('uncheck')
                status = 0;
            }else{
                $('.act').iCheck('check')   
                status = 1;
            }

            post  = $(".act").map(function(e){
                d     = $(this).data();
                group = parseInt($('#group').select2('val'));
                a = {
                    menu_id: d.menu_id,
                    detail_id: d.detail_id,
                    privilege: d.privilege,
                    group: group,
                    status: status,
                }
                return a;
            }).toArray();
            send = {params : post}
            savePrivilege(send)
        });
    });
</script>