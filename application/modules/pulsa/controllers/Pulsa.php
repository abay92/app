<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * -------------------
 * CLASS NAME : Pulsa
 * -------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class Pulsa extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('pulsa_model');
        $this->module = 'pulsa';
        $this->_table = 't_trx_pulsa';
        $this->_sess  = $this->session->userdata('app_session');
        $this->_code  = decode($this->_sess['code']);
    }
 	
 	function index() {
 		$data['title']   = "Transaksi Pulsa";
 		$data['content'] = "pulsa/index";
        $data['year']    = $this->pulsa_model->list_year($this->_code);
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');

        $this->load->view('template/page', $data);
    }

    function add(){
        validate_ajax();
        $data['title']       = 'Tambah Transaksi';
        $data['date']        = date('d F Y');
        $data['income_type'] = $this->pulsa_model->dropdown_income_type();

        $this->load->view('pulsa/add',$data);
    }

    function edit($param=''){
        validate_ajax();
        $data['title']       = 'Edit Pemasukan';
        $data['income_type'] = $this->pulsa_model->dropdown_income_type();
        $data['row']         = $this->mglobal->selectDataByID($this->_table, 'income_id', decode($param));
        $data['id']          = $param;
        $data['incomeType']  = $data['row']->income_type;
        $data['date']        = date('d F Y', strtotime($data['row']->date));
        $data['friday']      = date('d F Y', strtotime('previous friday') );
        $data['income']      = number_format($data['row']->income,0,',','.');
        $this->load->view('income/edit',$data);
    }

    function action_add(){
        validate_ajax();
        $post   = $this->input->post();

        /* validation */
        $this->form_validation
        ->set_rules('customer_name', 'Nama Pelanggan', 'trim|required')
        ->set_rules('phone', 'Nomor HP Pelanggan', 'trim|required')
        ->set_rules('date', 'Tanggal Transaksi', 'trim|required')
        ->set_rules('purchase_price', 'Harga Beli', 'trim|required')
        ->set_rules('selling_price', 'Harga Jual', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* data post */

        if(isset($post['now'])){
            $data = array(
                'customer_name' => strtoupper($post['customer_name']),
                'phone' => $post['phone'],
                'purchase_price' => $post['purchase_price'],
                'selling_price' => $post['selling_price'],
                'booking_date' => date('Y-m-d', strtotime($post['date'])),
                'payment_date' => date('Y-m-d', strtotime($post['date'])),
            );
        }else{
            $data = array(
                'customer_name' => strtoupper($post['customer_name']),
                'phone' => $post['phone'],
                'purchase_price' => $post['purchase_price'],
                'selling_price' => $post['selling_price'],
                'booking_date' => date('Y-m-d', strtotime($post['date'])),
            );
        }
       

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }else{
            $save = $this->mglobal->saveData($this->_table, $data);
            if($save){
                $response = json_api('success','Simpan Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;     
    }

    function action_edit(){
        validate_ajax();
        $post   = $this->input->post();

        /* validation */
        $this->form_validation
        ->set_rules('income_type', 'Jenis Pemasukan', 'trim|required')
        ->set_rules('income', 'Pemasukan', 'trim|required')
        ->set_rules('date', 'Tanggal', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* data post */
        $data = array(
            'income_id'   => decode($post['id']),
            'income_type' => $post['income_type'],
            'income' => $post['income'],
            'date' => date('Y-m-d', strtotime($post['date'])),
            'note' => $post['note'],
            'company_code' => $this->_code
        );

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }else{
            $update = $this->mglobal->updateData($this->_table, $data, 'income_id');

            if($update){
                $response = json_api('success','Update Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;
    }

    function delete($param=''){
        validate_ajax();
        $delete = $this->mglobal->deleteDataById($this->_table,'pulsa_id',decode($param));
        if($delete){
            $response =  json_api('success','Delete Data Berhasil');
        }else{
            $response =  json_encode($this->db->error()); 
        }    

        $data = array(
            'pulsa_id'=> decode($param)
        );

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log); 

        echo $response;  
    }

    function action_pay($param=''){
        validate_ajax();

        $data = array(
            'pulsa_id'=> decode($param),
            'payment_date' => date('Y-m-d')
        );

        $update = $this->mglobal->updateData($this->_table,$data,'pulsa_id');
        if($update){
            $response = json_api('success','Berhasil diupdate');
        }else{
            $response = json_encode($this->db->error()); 
        }    

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);   

        echo $response;
    }

    function get_list(){
    	validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->pulsa_model->get_list($this->_code);
        $bayar     = '<span class="label bg-green">Sudah dibayar</span>';
        $no_bayar  = '<span class="label bg-red">Belum dibayar</span>';

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                $r->pulsa_id       = encode($r->pulsa_id);
                $r->status         = $no_bayar;
                $r->booking_date   = date_indo($r->booking_date);
                $r->customer_name  = strtoupper($r->customer_name);
                $r->purchase_price = number_format($r->purchase_price,0,',','.');
                $r->selling_price  = number_format($r->selling_price,0,',','.');
                $r->laba           = number_format($r->laba,0,',','.');

                if($r->payment_date != null || $r->payment_date != ''){
                    $r->payment_date= date_indo($r->payment_date);
                    $r->status      = $bayar;
                    $r->action      = '';
                }else{
                    $r->action      = createBtnAction($this->module,'edit',$r->pulsa_id);
                    if(checkAccess('pulsa','change_status')){
                        $act = site_url('pulsa/action_pay/').$r->pulsa_id;
                        $r->action .= '<button onclick="confirmationAction(\'Apakah Anda Yakin Pelanggan ini Sudah Bayar?\',\''.$act.'\')" class="btn btn-success btn-icon btn-xs" title="Sudah Bayar"><i class="fa fa-check"></i></button> ';
                    }
                }

                $r->action     .= createBtnAction($this->module,'delete',$r->pulsa_id);
                
                $rows_data[]  = $r;
                unset($r->pulsa_id);
            }
        }

        $data['total']    = $dataList['total'];
        $data['rows']     = $rows_data;
        $data['income']   = number_format($dataList['income'],0,',','.');
        
        echo json_encode($data);
    }
}
