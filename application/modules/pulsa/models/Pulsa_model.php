<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pulsa_model extends CI_Model {
	function get_list($code) {
		$data 	 = array();
		$p_date  = $this->input->post('payment_date');
		$b_date  = $this->input->post('booking_date');

		$search  = trim(strtoupper($this->db->escape_like_str($this->input->post('search'))));
		$status  = $this->input->post('status');
		$page 	 = $this->input->post('page') ? $this->input->post('page') : 1;
		$rows 	 = $this->input->post('rows') ? $this->input->post('rows') : 10;
		$offset  = ($page - 1) * $rows;
		$sort 	 = $this->input->post('sort') ? $this->input->post('sort') : 'payment_date';
		$order 	 = $this->input->post('order') ? strtoupper($this->input->post('order')) : 'ASC';
		$payment_date  = ($p_date == '') ? '' : date('Y-m-d', strtotime($p_date));
		$booking_date  = ($b_date == '') ? '' : date('Y-m-d', strtotime($b_date));
		$income  = 0;

		$where 	 = "WHERE active IN (0,1)";

		if($status == 1){
			$where .= " AND payment_date IS NOT null";
		}

		if($status == 2){
			$where .= " AND payment_date IS null";
		}

		$iLike = " AND (UPPER(customer_name) LIKE '%{$search}%' OR UPPER(phone) LIKE '%{$search}%')";
		if($search != ''){
			$where .= $iLike;
		}

		if($payment_date != '' && $booking_date != ''){
			if($search != ''){
				$where .= " {$iLike} AND booking_date = '{$booking_date}' AND payment_date = '{$payment_date}'";
			}else{
				$where .= " AND booking_date = '{$booking_date}' AND payment_date = '{$payment_date}'";
			}
		}elseif($payment_date == '' && $booking_date != ''){
			if($search != ''){
				$where .= " {$iLike} AND booking_date = '{$booking_date}'";
			}else{
				$where .= " AND booking_date = '{$booking_date}'";
			}
		}elseif($payment_date != '' && $booking_date == ''){
			if($search != ''){
				$where .= " {$iLike} AND payment_date = '{$payment_date}'";
			}else{
				$where .= " AND payment_date = '{$payment_date}'";
			}
		}

		// if($search != '' AND $payment_date != '' AND $booking_date != ''){
		// 	$where .= " AND booking_date = '{$booking_date}' AND payment_date = '{$payment_date}' AND (UPPER(customer_name) LIKE '%{$search}%' OR UPPER(phone) LIKE '%{$search}%')";
		// }

		// elseif($search != '' AND $payment_date == '' AND $booking_date == ''){
		// 	$where .= " AND (UPPER(customer_name) LIKE '%{$search}%' OR UPPER(phone) LIKE '%{$search}%')";
		// }

		// elseif($search != '' AND $payment_date != '' AND $booking_date == ''){
		// 	$where .= " AND payment_date = '{$payment_date}' AND (UPPER(customer_name) LIKE '%{$search}%' OR UPPER(phone) LIKE '%{$search}%')";
		// }

		// elseif($search != '' AND $payment_date == '' AND $booking_date != ''){
		// 	$where .= " AND booking_date = '{$booking_date}' AND (UPPER(customer_name) LIKE '%{$search}%' OR UPPER(phone) LIKE '%{$search}%')";
		// }

		$sql 		= "SELECT 
						pulsa_id, 
						booking_date,
						payment_date,
						customer_name,
						phone,
						purchase_price,
						selling_price,
						(selling_price - purchase_price) AS laba,
						active 
					FROM t_trx_pulsa {$where} ORDER BY {$sort} {$order}";
		$query 		= $this->db->query($sql);
		$total_rows = $query->num_rows();

		foreach ($this->db->query($sql)->result() as $key => $value) {
			if($value->payment_date != null || $value->payment_date != ''){
				$income += $value->laba;
			}
		}

		$sql 	   .= " LIMIT {$rows} OFFSET {$offset}";
		$query2		= $this->db->query($sql);
				
		$data['total'] 	= $total_rows;
		$data['income'] = $income;
		$data['rows']  	= $query2->result();
		
		return $data;
	}

	function dropdown_income_type(){
		$sql 		= "SELECT lookup_value, lookup_desc FROM t_mtr_lookup WHERE LOWER(lookup_key) = 'income_type'";
        $query 		= $this->db->query($sql)->result();
        $data 		= array();
        $data[''] 	= '';

        if($query){
        	foreach($query as $row){
	         	$data[$row->lookup_value] = $row->lookup_desc;
	        }
        }

        return $data;
    }

    function list_year($code){
    	$sql = "SELECT DISTINCT SUBSTRING_INDEX(booking_date, '-', 1) AS year 
			   	FROM t_trx_pulsa ORDER BY booking_date ASC";	
		
        $query 		= $this->db->query($sql)->result();
        $year 		= date('Y');
        $data[$year]= $year;

        if($query){
        	foreach($query as $row){
        		if($row->year != $year){
	         		$data[$row->year] = $row->year;
        		}
	        }
        }

        return $data;
    }
}