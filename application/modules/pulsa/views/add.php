<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('pulsa/action_add', 'id="ff" autocomplete="on"'); ?>
        <div class="box-body">
            <div class="form-group">
                 <div class="row">
                    <div class="col-sm-6">
                        <label>Nama Pelanggan</label>
                        <input type="text" name="customer_name" class="form-control" placeholder="Nama Pelanggan" required>
                    </div>
                    <div class="col-sm-6">
                        <label>Nomor Handphone</label>
                        <input type="text" class="form-control my-form-control my-font-small" name="phone" placeholder="Nomor Handphone" required onkeypress="return isNumberKey(event)" minlength="10">
                    </div>
                </div>
            </div>
            <div class="form-group">
                 <div class="row">
                    <div class="col-sm-4">
                        <label>Harga Beli</label>
                        <input type="text" pattern="[0-9]*" name="purchase_price" class="form-control nominal" placeholder="Nilai Pemasukan" required>
                    </div>
                    <div class="col-sm-4">
                        <label>Harga Jual</label>
                        <input type="text" pattern="[0-9]*" name="selling_price" class="form-control nominal" placeholder="Nilai Pemasukan" required>
                    </div>
                    <div class="col-sm-4">
                        <label>Tanggal Transaksi</label>
                        <div class="input-group">
                            <input type="text" name="date" class="form-control pull-right" id="date" value="<?php echo $date; ?>" required>
                            <div class="input-group-addon date" style="cursor: pointer">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="checkbox icheck">
                        <label>
                          <input type="checkbox" name="now" id="now"> Langsung Bayar
                        </label>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Simpan') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#now').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });

        $.extend($.fn.datepicker.defaults, {
            format: 'dd MM yyyy',
            endDate: new Date(),
            autoclose: true,
            orientation: 'auto bottom'
        });

        $('#date').datepicker()

        $('.nominal').keyup(function(e){
            this.value = formatRupiah(this.value);
        })

        rules = {};
        validateForm('#ff',function(url,data){
           data.selling_price = removeRupiah(data.selling_price)
           data.purchase_price = removeRupiah(data.purchase_price)
           postData(url,data);
       });
    })
</script>