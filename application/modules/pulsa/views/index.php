<div class="content-wrapper">
	<section class="content">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $title ?></h3>
                <div class="box-tools pull-right">
                    <?php echo $btn_add ?>
                </div>
            </div>
            <div id="tb">
                <div class="col-md-3 pad-top">
                    <div class="form-group mar-bottom">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">Status</span>
                            <select class="form-control input-sm" id="status">
                                <option value=0>-- Semua --</option>
                                <option value=1>Sudah dibayar</option>
                                <option value=2>Belum dibayar</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 pad-top">
                    <div class="form-group mar-bottom">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">Tanggal Pesan</span>
                            <input type="text" class="form-control input-sm" id="booking_date" placeholder="Tanggal" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 pad-top">
                    <div class="form-group mar-bottom">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">Tanggal Bayar</span>
                            <input type="text" class="form-control input-sm" id="payment_date" placeholder="Tanggal" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 pad-top">
                    <div class="form-group mar-bottom">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Cari Nama, No. Hp" id="search" autocomplete="off">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-flat" id="btnSearch">Cari</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12"><hr></div>
                <div class="col-md-12"><h4 id="incomes">Keuntungan Rp. 0,-</h4></div>
            </div>
            <div class="box-body no-padding">
                <table class="table" id="grid"></table>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $.extend($.fn.datepicker.defaults, {
            format: 'dd MM yyyy',
            endDate: new Date(),
            autoclose: true,
            orientation: 'auto bottom'
        });

        $('#booking_date').datepicker()
        $('#payment_date').datepicker()

        settingDefaultDatagrid()
        $('#grid').datagrid({
            url     : baseURL+'pulsa/get_list',
            queryParams : {
                search: $('#search').val(),
                status : $('#status').val(),
                booking_date : $('#booking_date').val(),
                payment_date : $('#payment_date').val()
            },
            columns : [[
                { field: 'customer_name', title: 'Nama Pelanggan', width: 100, sortable  : true},
                { field: 'phone', title: 'Nomor Pelanggan', width: 100, sortable  : true},
                { field: 'purchase_price', title: 'Harga Beli (Rp.)', width: 100, sortable  : true},
                { field: 'selling_price', title: 'Harga Jual (Rp.)', width: 100, sortable  : true},
                { field: 'laba', title: 'Laba (Rp.)', width: 100, sortable  : true},
                { field: 'booking_date', title: 'Tanggal Beli', width: 100, sortable  : true},
                { field: 'payment_date', title: 'Tanggal Bayar', width: 100, sortable  : true},
                { field: 'status', title: 'Status', width: 100, align: 'center', sortable  : true},
                { field: 'action', title: 'Aksi', width: 100, align: 'center'}
            ]],

            onLoadSuccess: function(data){
                $('#grid').datagrid('resize');
                $('.sidebar-toggle').click(function(){
                    setTimeout(function(){
                        $('#grid').datagrid('resize');
                    },400)
                });

                $(window).resize(function(){
                    setTimeout(function(){
                        $('#grid').datagrid('resize');
                    },400)
                })

                $('#incomes').html('Keuntungan Rp. '+data.income+',-')
            },
        })

        $('#btnSearch').click(function(){
            searchingDatagrid()
        })

        $('#year').change(function(){
            searchingDatagrid()
        })

        $('#status').change(function(){
            searchingDatagrid()
        })

        $('#search').keydown(function(e) {
            if (e.which == 13){
                searchingDatagrid()
            }
        })

        function searchingDatagrid(){
            $('#grid').datagrid('load',{
                search: $('#search').val(),
                status: $('#status').val(),
                booking_date : $('#booking_date').val(),
                payment_date : $('#payment_date').val()
            });
        }
    })
</script>