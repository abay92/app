<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * -------------------
 * CLASS NAME : Screen
 * -------------------
 *
 * @author     Robai <abayr92@gmail.com>
 * @copyright  2018
 *
 */

class Screen extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('screen_model');
    }

    function friday($param='') {
        if ($param == '') {
            show_404();
            return false;
        }

        if(!checkUrlAccess('dashboard')){
            $this->load->view('template/403');
            return false;
        } 

        $data['title']= "Pemasukan Infaq Sholat Jum'at";
        $data['js']   = "Jadwal Sholat";
        $data['sj']   = "Jadwal Khutbah Sholat Jum'at";
        $data['param']= $param;
        $data['year'] = $this->screen_model->list_year(decode($param));

        $this->load->view('screen/index',$data);
    }

    function list_income_friday($param=''){
        if ($param == '') {
            show_404();
            return false;
        }

        validate_ajax();
        $year    = $this->input->post('year');
        $data = array();

        for ($i=1; $i < 13; $i++) { 
            $month  = $i < 10 ? '0'.$i : $i;
            $ar['month']  = month_indo($month);
            $ar['friday'] = $this->screen_model->list_income_friday($year,$month,decode($param));

            $data[] = $ar; 
        }

        $arr['rows'] = $data;
        echo json_encode($arr);
    }

    function list_friday_officer($param=''){
        if ($param == '') {
            show_404();
            return false;
        }

        validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->screen_model->get_friday_officer(decode($param));

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                $r->date    = date_indo($r->date);
                $rows_data[]= $r;
            }
        }

        $data['total'] = $dataList['total'];
        $data['rows']  = $rows_data;
        
        echo json_encode($data);
    }

    function list_friday_income($param=''){
        if ($param == '') {
            show_404();
            return false;
        }

        validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->screen_model->list_income_friday_month(decode($param));

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                $r->date   = date_indo($r->date);
                $r->income = 'Rp. '.number_format($r->income,0,',','.').' -,';

                $rows_data[]= $r;
            }
        }

        $data['total'] = $dataList['total'];
        $data['rows']  = $rows_data;
        
        echo json_encode($data);
    }
}
