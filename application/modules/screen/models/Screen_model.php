<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Screen_model extends CI_Model {

    function list_income_friday($year,$month,$code){
    	$sql   = "SELECT income, date 
        FROM t_trx_income 
        WHERE income_type = '1' AND 
            YEAR(date) = $year AND 
            MONTH(date) = $month AND
            company_code = '$code'
        ORDER BY date ASC";
        $query = $this->db->query($sql)->result();

        $data = array();

        foreach ($query as $key => $value) {
        	$value->date   = date_indo($value->date);
        	$value->income = 'Rp. '.number_format($value->income,0,',','.').' -,';

        	$data[] = $value;
        }

        return $data;
    }

    function get_friday_officer($code){
		$data = array();
		$date = date('Y-m-d');
		
		$sql = "SELECT date, khotib, imam, muadzin 
        FROM t_mtr_khutbah 
        WHERE DATE(date) >= '$date' AND company_code = '$code'";
		$query 		   = $this->db->query($sql);
		$data['total'] = $query->num_rows();
		$data['rows']  = $query->result();

		return $data;
	}

    function list_income_friday_month($code){
        $data = array();
        $date = date('Y-m-d');

        $sql = "SELECT income, date 
                FROM t_trx_income 
                WHERE income_type = '1' AND income_type = '1' AND date <= '$date' AND company_code = '$code'
                ORDER BY date DESC LIMIT 5";
        $query         = $this->db->query($sql);
        $data['total'] = $query->num_rows();
        $data['rows']  = $query->result();

        return $data;
    }

    function list_year($code){
		$sql 		= "SELECT DISTINCT SUBSTRING_INDEX(date, '-', 1) AS year FROM t_trx_income WHERE company_code = '$code' ORDER BY date ASC LIMIT 5";
        $query 		= $this->db->query($sql)->result();
        $year 		= date('Y');
        $data[$year]= $year;

        if($query){
        	foreach($query as $row){
        		if($row->year != $year){
	         		$data[$row->year] = $row->year;
        		}
	        }
        }

        return $data;
    }
}