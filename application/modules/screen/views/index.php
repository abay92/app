<?php echo $this->load->view('template/assets'); ?>
<link href="<?php echo config_item('css'); ?>screen.css" rel="stylesheet">
<!-- <link href='https://fonts.googleapis.com/css?family=Orbitron' rel='stylesheet' type='text/css'> -->
<!-- <section class="content col-md-8">
    <div class="box box-default">
        <div class="box-header with-border" style="padding-top: 0px;">
            <h3><b><?php //echo $title ?></b></h3>
        </div>
        <div id="tb">
            <div class="padd-tb">
                <div class="col-md-2 input-group input-group-sm">
                    <?php //echo form_dropdown('', $year, '', 'class="form-control input-sm" id="year" style="border-radius: 3px;"'); ?>
                </div>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class="table" id="grid"></table>
        </div>
    </div>
</section> -->
<section class="content col-md-5">
    <!-- <div class="col-md-12">
        <div class="box box-default">
            <div class="box-header with-border" style="padding-top: 0px; background-color: #3c8dbc; color: white">
                <h2 style="float: right; font-family: 'digital-7', sans-serif;"><b id="clock"></b></h2>
            </div>
            <div class="box-body no-padding"></div>
        </div>
    </div> -->

    <div class="col-md-12">
        <div class="box box-default">
           <!--  <div class="box-header with-border" style="padding-top: 0px;">
                <h3><b><?php //echo $js ?></b></h3>
            </div> -->
            <div class="box-body no-padding">
                <iframe src="//www.arrahmah.com/jadwal-shalat/?bg=097900" scrolling="no" width="100%" height="200" frameborder="no" framespacing="0"></iframe>
                <!-- <script type="text/javascript" src="http://www.mahesajenar.com/scripts/adzan.php?kota=Indramayu&type=text1"> </script> -->
                <!-- <iframe src="http://www.republika.co.id/jadwal-sholat/" scrolling="no" width="550" height="250" frameborder="no" framespacing="0"></iframe>  -->
                <!-- <iframe src="https://www.jadwalsholat.org/adzan/ajax.row.php?id=82" frameborder="0" width="200" height="200"></iframe> -->
                <!-- <iframe src="https://www.jadwalsholat.org/hijri/hijri.php" frameborder="0" width="310" height="300"></iframe> -->
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-header with-border" style="padding: 0 5px 0 5px">
                <b><?php echo $sj ?></b>
            </div>
            <div class="box-body no-padding">
                <table class="table" id="grid2"></table>
            </div>
        </div>
    </div>

     <div class="col-md-12">
        <div class="box box-default">
            <div class="box-header with-border" style="padding: 0 5px 0 5px">
                <b><?php echo $title ?></b>
            </div>
            <div class="box-body no-padding">
                <table class="table" id="grid3"></table>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        // date_time('clock');

        settingDefaultDatagrid()
        // $('#grid').datagrid({
        //     url     : '<?php echo site_url('screen/list_income_friday/').''.$param ?>',
        //     queryParams : {year : $('#year').val()},
        //     rownumbers  : false,
        //     pagination  : false,

        //     columns : [[
        //         { field: 'month', title: 'Bulan', width: 12},
        //         { field: 'friday', title: 'Hasil', width: 100, formatter: function(value, row, index) {
        //             table = '<table class="detail"><tbody><tr class="tr-b">';

        //             for (i = 0; i < value.length; i++){
        //               table += '<td class="trc"> '+value[i].date+' </td>';
        //           }

        //           table += '</tr><tr class="trc">';

        //           for (a = 0; a < value.length; a++){
        //               table += '<td class="trc"> '+value[a].income+' </td>';
        //           }

        //           table += '</tr></tbody></table>';

        //           return table;
        //       }},
        //     ]],

        //     onLoadSuccess: function(data){
        //         $(window).resize(function(){
        //             setTimeout(function(){
        //                 $('#grid').datagrid('resize');
        //             },400)
        //         })

        //         // setTimeout(function(){
        //         //     searchingDatagrid()
        //         // },1000)
        //     },
        // })

        // $('#btnSearch').click(function(){
        //     searchingDatagrid()
        // })

        // $('#year').change(function(){
        //     searchingDatagrid()
        // })

        // $('#search').keydown(function(e) {
        //     if (e.which == 13){
        //         searchingDatagrid()
        //     }
        // })

        $('#grid2').datagrid({
            url     : '<?php echo site_url('screen/list_friday_officer/').''.$param ?>',
            rownumbers  : false,
            pagination  : false,
            toolbar     : '',
            minHeight   : '100px',

            columns : [[
                { field: 'date', title: 'Tanggal', width: 120},
                { field: 'imam', title: 'Imam', width: 100},
                { field: 'khotib', title: 'Khotib', width: 100},
                { field: 'muadzin', title: 'Muadzin', width: 90},
            ]],

            onLoadSuccess: function(data){
                $('#grid2').datagrid('resize');                
                $(window).resize(function(){
                    setTimeout(function(){
                        $('#grid2').datagrid('resize');
                    },400)
                })
            },
        })

         $('#grid3').datagrid({
            url     : '<?php echo site_url('screen/list_friday_income/').''.$param ?>',
            rownumbers  : false,
            pagination  : false,
            toolbar     : '',
            minHeight   : '100px',

            columns : [[
                { field: 'date', title: 'Tanggal', width: 100},
                { field: 'income', title: 'Pemasukan', width: 100},
            ]],

            onLoadSuccess: function(data){
                $('#grid3').datagrid('resize');                
                $(window).resize(function(){
                    setTimeout(function(){
                        $('#grid3').datagrid('resize');
                    },400)
                })
            },
        })
    })

    function searchingDatagrid(){
        $('#grid').datagrid('load',{
            year  : $('#year').val(),
        });
    }

    function date_time(id){
        date    = new Date;
        year    = date.getFullYear();
        month   = date.getMonth();
        months  = new Array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        d       = date.getDate();
        day     = date.getDay();
        days    = new Array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');

        h = date.getHours();
        if(h < 10){
          h = "0"+h;
        }

        m = date.getMinutes();
        if(m<10){
          m = "0"+m;
        }

        s = date.getSeconds();
        if(s<10){
          s = "0"+s;
        }

        result = ''+days[day]+', '+d+' '+months[month]+' '+year+' '+h+':'+m+':'+s;

        document.getElementById(id).innerHTML = result;

        setTimeout('date_time("'+id+'");','1000');

        return true;
    }
</script>