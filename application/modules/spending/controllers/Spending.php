<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ---------------------
 * CLASS NAME : Spending
 * ---------------------
 *
 * @author     Robai <abayr92@gmail.com>
 * @copyright  2018
 *
 */

class Spending extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('spending_model');
        $this->module = 'spending';
        $this->_table = 't_trx_spending';
        $this->_sess  = $this->session->userdata('app_session');
        $this->_code  = decode($this->_sess['code']);
    }
 	
 	function index() {
 		$data['title']   = "Pengeluaran";
 		$data['content'] = "spending/index";
        $data['year']    = $this->spending_model->list_year($this->_code);
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');

        $this->load->view('template/page', $data);
    }

    function add(){
        validate_ajax();
        $data['title']= 'Tambah Pengeluaran';
        $data['date'] = date('d F Y');

        $this->load->view('spending/add',$data);
    }

    function edit($param=''){
        validate_ajax();
        $data['title']    = 'Edit Pengeluaran';
        $data['row']      = $this->mglobal->selectDataByID($this->_table, 'spending_id', decode($param));
        $data['id']       = $param;
        $data['date']     = date('d F Y', strtotime($data['row']->date));
        $data['spending'] = number_format($data['row']->spending,0,',','.');

        $this->load->view('spending/edit',$data);
    }

    function action_add(){
        validate_ajax();
        $post   = $this->input->post();

        /* validation */
        $this->form_validation
        ->set_rules('spending', 'Pengeluaran', 'trim|required')
        ->set_rules('date', 'Tanggal', 'trim|required')
        ->set_rules('note', 'Catatan', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* data post */
        $data = array(
            'spending' => $post['spending'],
            'date' => date('Y-m-d', strtotime($post['date'])),
            'note' => $post['note'],
            'company_code' => $this->_code
        );

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }else{
            $save = $this->mglobal->saveData($this->_table, $data);
            if($save){
                $response = json_api('success','Simpan Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;     
    }

    function action_edit(){
        validate_ajax();
        $post   = $this->input->post();

        /* validation */
        $this->form_validation
        ->set_rules('spending', 'Pengeluaran', 'trim|required')
        ->set_rules('date', 'Tanggal', 'trim|required')
        ->set_rules('note', 'Catatan', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* data post */
        $data = array(
            'spending_id'   => decode($post['id']),
            'spending' => $post['spending'],
            'date' => date('Y-m-d', strtotime($post['date'])),
            'note' => $post['note'],
            'company_code' => $this->_code
        );

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }else{
            $update = $this->mglobal->updateData($this->_table, $data, 'spending_id');

            if($update){
                $response = json_api('success','Update Data Berhasil');
            }else{
                $response = json_encode($this->db->error());
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;
    }

    function delete($param=''){
        validate_ajax();
        $delete = $this->mglobal->deleteDataById($this->_table,'spending_id',decode($param));
        if($delete){
            $response =  json_api('success','Delete Data Berhasil');
        }else{
            $response =  json_encode($this->db->error()); 
        }    

        $data = array(
            'spending_id'=> decode($param)
        );

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log); 

        echo $response;  
    }

    function get_list(){
    	validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->spending_model->get_list($this->_code);

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                $r->spending_id = encode($r->spending_id);
                $r->date        = date_indo($r->date);
                $r->spending    = number_format($r->spending,0,',','.');
                $r->action      = createBtnAction($this->module,'edit',$r->spending_id).' ';
                $r->action     .= createBtnAction($this->module,'delete',$r->spending_id);

                $rows_data[]= $r;
                unset($r->spending_id);
            }
        }

        $data['total'] = $dataList['total'];
        $data['rows']  = $rows_data;
        $data['totals'] = number_format($dataList['totals'],0,',','.');
        
        echo json_encode($data);
    }
}
