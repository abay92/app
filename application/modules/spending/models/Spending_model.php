<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Spending_model extends CI_Model {
	function get_list($code) {
		$data 	 = array();
		$search  = trim(strtoupper($this->db->escape_like_str($this->input->post('search'))));
		$year  	 = $this->input->post('year');
		$page 	 = $this->input->post('page') ? $this->input->post('page') : 1;
		$rows 	 = $this->input->post('rows') ? $this->input->post('rows') : 10;
		$offset  = ($page - 1) * $rows;
		$sort 	 = $this->input->post('sort') ? $this->input->post('sort') : 'date';
		$order 	 = $this->input->post('order') ? strtoupper($this->input->post('order')) : 'DESC';

		$where 	 = "WHERE active = 1 AND YEAR(date) = '{$year}' AND company_code = '$code'";		
		
		if($search != ''){
			$where .= " AND (UPPER(note) LIKE '%{$search}%')";
		}

		$sql = "SELECT spending_id, spending, note, active, date FROM t_trx_spending
				{$where} ORDER BY {$sort} {$order}";
		
		$query 		= $this->db->query($sql);
		$total_rows = $query->num_rows();
		$totals 	= 0;

		foreach ($this->db->query($sql)->result() as $key => $value) {
			$totals += $value->spending;
		}

		$sql 	   .= " LIMIT {$rows} OFFSET {$offset}";
		$query2		= $this->db->query($sql);
				
		$data['total'] 	= $total_rows;
		$data['totals'] = $totals;
		$data['rows']  	= $query2->result();
		
		return $data;
	}

    function list_year($code){
		$sql 		= "SELECT DISTINCT SUBSTRING_INDEX(date, '-', 1) AS year FROM t_trx_spending WHERE company_code = '$code' ORDER BY date ASC";
        $query 		= $this->db->query($sql)->result();
        $year 		= date('Y');
        $data[$year]= $year;

        if($query){
        	foreach($query as $row){
        		if($row->year != $year){
	         		$data[$row->year] = $row->year;
        		}
	        }
        }

        return $data;
    }
}