<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('spending/action_edit', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                 <div class="row">
                    <div class="col-sm-6">
                        <label>Tanggal</label>
                        <div class="input-group group">
                            <input type="text" name="date" class="form-control pull-right in-group" id="date" value="<?php echo $date; ?>" required>
                            <div class="input-group-addon date" style="cursor: pointer">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label>Nilai Pengeluaran (Rp)</label>
                        <input type="text" pattern="[0-9]*" name="spending" class="form-control spending focus" placeholder="Nilai Pengeluaran" required value="<?php echo $spending ?>">
                        <input type="hidden" name="id" value="<?php echo $id ?>">                        
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Catatan</label>
                        <textarea name="note" class="form-control" rows="5" placeholder="Catatan" style="resize: none;"><?php echo $row->note ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Update') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $.extend($.fn.datepicker.defaults, {
            format: 'dd MM yyyy',
            endDate: new Date(),
            autoclose: true,
            orientation: 'auto bottom'
        });

        $('#date').datepicker()
        $('.date').click(function (e) {
            $('#date').datepicker().focus();
            e.preventDefault();
        });

        $('#date').datepicker().on('changeDate', function(e) {
            $(this).valid();
        });

        $('.spending').keyup(function(e){
            this.value = formatRupiah(this.value);
        })

        validateForm('#ff',function(url,data){
           data.spending = removeRupiah(data.spending)
           postData(url,data);
       });
    })
</script>