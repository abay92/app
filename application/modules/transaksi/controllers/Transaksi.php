<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * --------------------
 * CLASS NAME : Khutbah
 * --------------------
 *
 * @author     Robai <abayr92@gmail.com>
 * @copyright  2018
 *
 */

class Transaksi extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('transaksi_model');
        $this->module = 'transaksi';
        $this->_table = 't_mtr_khutbah';
        $this->_sess  = $this->session->userdata('app_session');
        $this->_code  = decode($this->_sess['code']);
    }
 	
 	function index() {
 		$data['title']   = "Transaksi";
 		$data['content'] = "transaksi/index";
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');

        $this->load->view('template/page', $data);
    }
}
