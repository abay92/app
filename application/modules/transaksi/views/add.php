<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('khutbah/action_add', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                 <div class="row">
                    <div class="col-sm-6">
                        <label>Tanggal</label>
                        <div class="input-group">
                            <input type="text" name="date" class="form-control pull-right" id="date" required>
                            <div class="input-group-addon date" style="cursor: pointer">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label>Imam</label>
                        <input type="text" name="imam" class="form-control" placeholder="Nama Imam" required>
                    </div>
                </div>
            </div>
         <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Khotib</label>
                        <input type="text" name="khotib" class="form-control" placeholder="Nama Khotib" required>
                    </div>
                    <div class="col-sm-6">
                        <label>Muadzin</label>
                        <input type="text" name="muadzin" class="form-control" placeholder="Nama Muadzin" required>
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Simpan') ?>\
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $.extend($.fn.datepicker.defaults, {
            format: 'dd MM yyyy',
            autoclose: true,
            orientation: 'auto bottom',
            daysOfWeekDisabled: [0,1,2,3,4,6],
            startDate: '<?php echo $friday ?>'
        });

        $('#date').datepicker().on('changeDate', function(e) {
            $(this).valid();
        });

        $('#date').datepicker()
        $('.date').click(function (e) {
            $('#date').datepicker().focus();
            e.preventDefault();
        });

        validateForm('#ff',function(url,data){
           postData(url,data);
       });
    })
</script>