<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('khutbah/action_edit', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                 <div class="row">
                    <div class="col-sm-6">
                        <label>Tanggal</label>
                        <div class="input-group">
                            <input type="text" name="date" class="form-control pull-right" id="date" value="<?php echo $date; ?>" required>
                            <div class="input-group-addon date" style="cursor: pointer">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label>Imam</label>
                        <input type="text" name="imam" class="form-control" placeholder="Nama Imam" value="<?php echo $row->imam; ?>" required>
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                    </div>
                </div>
            </div>
         <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Khotib</label>
                        <input type="text" name="khotib" class="form-control" placeholder="Nama Khotib" value="<?php echo $row->khotib; ?>" required>
                    </div>
                    <div class="col-sm-6">
                        <label>Muadzin</label>
                        <input type="text" name="muadzin" class="form-control" placeholder="Nama Muadzin" value="<?php echo $row->muadzin; ?>" required>
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Update') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $.extend($.fn.datepicker.defaults, {
            format: 'dd MM yyyy',
            autoclose: true,
            orientation: 'auto bottom',
            daysOfWeekDisabled: [0,1,2,3,4,6]
        });

        $('#date').datepicker().on('changeDate', function(e) {
            $(this).valid();
        });

        $('#date').datepicker()
        $('.date').click(function (e) {
            $('#date').datepicker().focus();
            e.preventDefault();
        });

        validateForm('#ff',function(url,data){
           postData(url,data);
       });
    })
</script>