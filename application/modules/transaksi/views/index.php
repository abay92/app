<style type="text/css">
	.padd{
		padding: 15px;
	}
</style>
<div class="content-wrapper">
	<section class="content">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $title ?></h3>
			</div>
			<div class="box-body">
	         	<div class="row">
	         		<div class="col-sm-3">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<i class="fa fa-file-text-o fa-fw"></i> Informasi Nota
							</div>
							<div class="panel-body padd">
								<div class="form-horizontal">
									<div class="form-group">
										<label class="col-sm-4 control-label">No. Nota</label>
										<div class="col-sm-8">
											<input type="text" name="nomor_nota" class="form-control input-sm" id="nomor_nota" value="5BEF8BC086C251">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">Tanggal</label>
										<div class="col-sm-8">
											<input type="text" name="tanggal" class="form-control input-sm" id="tanggal" value="2018-11-17 04:32:16">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">Kasir</label>
										<div class="col-sm-8">
											<select name="id_kasir" id="id_kasir" class="form-control input-sm">
												<option value="1" selected="">Bang Admin</option><option value="2">Centini</option><option value="5">Jaka Sembung</option><option value="6">Joko Haji</option>										</select>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
	          	</div>
	      	</div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.sidebar-toggle').trigger('click')
    })
</script>