<?php 

/**
 * ------------------------------
 * NAME FILE : add.php - form add
 * ------------------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

?>

<div class="col-md-6 col-md-offset-3">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('user/action_add', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Nama Lengkap</label>
                        <input type="text" name="fullname" class="form-control" placeholder="Nama Lengkap" required>
                    </div>
                    <div class="col-sm-6">
                        <label>User Grup</label>
                        <?php echo form_dropdown('usergroup', $usergroup, '', 'class="form-control select2" required data-placeholder="Pilih User Grup"'); ?>
                    </div>
                </div>
            </div>

             <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control" placeholder="Username" required>
                    </div>
                    <div class="col-sm-6">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                    </div>
                </div>
            </div>

            <?php if($code== 'KSRRYT'){ ?>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Perusahaan</label>
                            <?php echo form_dropdown('company', $company, '', 'class="form-control select2" required data-placeholder="Pilih Perusahaan"'); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="box-footer text-right">
            <?php echo createBtnForm('Simpan') ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.mfp-wrap').removeAttr('tabindex')
        $('.select2').select2()
        $('.select2').change(function(){
            $(this).valid();
        })

        validatorPassword()
        rules    = { password : { password : true } };

        validateForm('#ff',function(url,data){
            data.password = CryptoJS.MD5(data.password).toString()
            postData(url,data);
        });
    })
</script>