<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * -----------------------
 * CLASS NAME : User_group
 * -----------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

class User_group extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('user_group_model');
        $this->module = 'user_group';
        $this->_table = 't_mtr_user_group';
    }
 	
 	function index() {
 		$data['title']   = "User Group";
 		$data['content'] = "user_group/index";
        $data['btn_add'] = createBtnAction($this->uri->uri_string(),'add');

        $this->load->view('template/page', $data);
    }

    function add(){
        validate_ajax();
        $data['title'] = 'Add User Group';
        $this->load->view('user_group/add',$data);
    }

    function edit($param=''){
        validate_ajax();
        $data['title'] = 'Edit User Group';
        $data['row']   = $this->mglobal->selectDataByID($this->_table, 'group_id', decode($param));
        $data['id']    = $param;
        $this->load->view('user_group/edit',$data);
    }

    function action_add(){
        validate_ajax();
        $post = $this->input->post();

        /* validation */
        $this->form_validation->set_rules('group', 'Nama Grup', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* data post */
        $data = array(
            'group_name' => strtoupper($post['group']),
        );

        $where = array('LOWER(group_name)' => strtolower($post['group']));
        $check = $this->mglobal->checkData($this->_table,$where);

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($check){
            $response = json_api('failed','Nama Group '.$post['group'].' Sudah Ada'); 
        }else{
            $save = $this->mglobal->saveData($this->_table, $data);
            if($save){
                $response = json_api('success','Simpan Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }  

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;     
    }

    function action_edit(){
        validate_ajax();
        $post   = $this->input->post();
        $id     = decode($post['id']);

        /* validation */
        $this->form_validation->set_rules('group', 'Nama Grup Name', 'trim|required');
        $this->form_validation->set_message('required','%s harus diisi!');

        /* data post */
        $data = array(
            'group_id'   => $id,
            'group_name' => strtoupper($post['group']),
        );

        /* check group name */
        $where = array('LOWER(group_name)' => strtolower($post['group']));
        $check = $this->mglobal->checkData($this->_table, $where, 'group_id', $id);

        if($this->form_validation->run() == FALSE){
            $response = json_api('failed',validation_errors());
        }elseif($check){
            $response = json_api('failed','Nama Grup '.$post['group'].' Sudah Ada'); 
        }else{
            $update = $this->mglobal->updateData($this->_table, $data, 'group_id');
            if($update){
                $response = json_api('success','Update Data Berhasil');
            }else{
                $response = json_encode($this->db->error()); 
            }
        }    

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log);

        echo $response;   
    }

    function delete($param=''){
        validate_ajax();
        $delete = $this->mglobal->deleteDataById($this->_table,'group_id',decode($param));
        if($delete){
            $response =  json_api('success','Delete Data Berhasil');
        }else{
            $response =  json_encode($this->db->error()); 
        }    

        $data = array(
            'user_id'=> decode($param)
        );

        /* data log */
        $log = array(
            'function'  => uri_string(),
            'request'   => json_encode($data),
            'response'  => $response,
            'ip_address'=> ipClient()
        );
        $this->mglobal->saveLog($log); 

        echo $response;  
    }

    function get_list(){
    	validate_ajax();
        $data      = array();
        $rows_data = array();
        $dataList  = $this->user_group_model->get_list();

        if($dataList['rows']){
            foreach ($dataList['rows'] as $key => $r) {
                $r->group_id = encode($r->group_id);
                $r->action   = createBtnAction($this->module,'edit',$r->group_id);

                $where = array('user_group' => decode($r->group_id));
                $check = $this->mglobal->checkData('t_mtr_user',$where);

                if(!$check){
                    $r->action .= createBtnAction($this->module,'delete',$r->group_id);
                }

                $rows_data[]= $r;
                unset($r->group_id);
            }
        }

        $data['total'] = $dataList['total'];
        $data['rows']  = $rows_data;
        
        echo json_encode($data);
    }
}
