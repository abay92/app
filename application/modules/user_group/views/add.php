<?php 

/**
 * ------------------------------
 * NAME FILE : add.php - form add
 * ------------------------------
 *
 * @author     Robai <robai.rastim@gmail.com>
 * @copyright  2018
 *
 */

?>

<div class="col-md-4 col-md-offset-4">
    <div class="box box-primary">
        <?php echo headerForm($title) ?>
        <?php echo form_open('user_group/action_add', 'id="ff" autocomplete="off"'); ?>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Group Name</label>
                        <input type="text" name="group" class="form-control focus" placeholder="Group Name" required>
                    </div>
                </div>
            </div>
        </div>
        <?php echo createBtnForm('Simpan') ?>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        validateForm('#ff',function(url,data){
           postData(url,data);
        });
    })
</script>