<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
<head>
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<title><?php echo config_item('name_web'); ?> - 404 Page not found</title>
	<link href='<?php echo config_item('icon'); ?>favicon.png' type='image/x-icon' rel='shortcut icon'>

	<link href="<?php echo config_item('bootstrap'); ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo config_item('font'); ?>font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo config_item('css'); ?>AdminLTE.min.css" rel="stylesheet">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper" style="background-color: #ecf0f5;">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        404 Error Page
      </h1>
    </section>
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
        	<br>
          	<h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
          	<h5><a href="<?php echo site_url('home') ?>">Back to Home</a></h5>
        </div>
      </div>
    </section>
  </div>
  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
