<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo config_item('name_web'); ?> - <?php echo $title; ?></title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link href='<?php echo config_item('icon'); ?>favicon.png' type='image/x-icon' rel='shortcut icon'>

	<!-- css -->
	<link href="<?php echo config_item('bootstrap'); ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo config_item('font'); ?>font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo config_item('font'); ?>Ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="<?php echo config_item('plugins'); ?>select2/css/select2.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo config_item('plugins'); ?>iCheck/square/blue.css" rel="stylesheet">
	<link href="<?php echo config_item('plugins'); ?>iCheck/all.css" rel="stylesheet">
	<link href="<?php echo config_item('plugins'); ?>alertify/css/alertify.core.css" rel="stylesheet">
	<link href="<?php echo config_item('plugins'); ?>alertify/css/alertify.default.css" rel="stylesheet" id="toggleCSS">
	<link href="<?php echo config_item('plugins'); ?>magnific-popup/magnific-popup.min.css" rel="stylesheet">
	<link href="<?php echo config_item('plugins'); ?>jquery-easyui-1.5.3/themes/metro/easyui.css" rel="stylesheet" type="text/css">
	<link href="<?php echo config_item('plugins'); ?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo config_item('css'); ?>AdminLTE.min.css" rel="stylesheet">
	<link href="<?php echo config_item('css'); ?>skins/_all-skins.min.css" rel="stylesheet">
	<link href="<?php echo config_item('css'); ?>global.css" rel="stylesheet">

	<!-- js -->
	<script src="<?php echo config_item('js'); ?>jquery/jquery.min.js"></script>
	<script src="<?php echo config_item('plugins'); ?>js-excel/xlsx.full.min.js" lang="javascript"></script>
    <script src="<?php echo config_item('plugins'); ?>js-excel/FileSaver.min.js" lang="javascript"></script>
	<script src="<?php echo config_item('bootstrap'); ?>js/bootstrap.min.js"></script>
	<script src="<?php echo config_item('plugins'); ?>alertify/js/alertify.min.js"></script>
	<script src="<?php echo config_item('plugins'); ?>iCheck/icheck.min.js"></script>
	<script src="<?php echo config_item('plugins'); ?>fastclick/lib/fastclick.js"></script>
	<script src="<?php echo config_item('plugins'); ?>magnific-popup/magnific-popup.min.js"></script>
    <script src="<?php echo config_item('plugins'); ?>jquery-easyui-1.5.3/jquery.easyui.min.js"></script>
    <script src="<?php echo config_item('plugins'); ?>select2/js/select2.full.min.js"></script>
    <script src="<?php echo config_item('plugins'); ?>bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo config_item('js'); ?>jquery.blockUI.js"></script>
	<script src="<?php echo config_item('js'); ?>validate.min.js"></script>
	<script src="<?php echo config_item('js'); ?>md5.js"></script>
	<script src="<?php echo config_item('js'); ?>adminlte.min.js"></script>
	<script src="<?php echo config_item('js'); ?>global.js"></script>	
</head>