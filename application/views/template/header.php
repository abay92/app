<?php
  $fullname = strtoupper($this->_sess['fullname']);
  if(strlen($fullname) > 15){
    $fullname = substr(strtoupper($this->_sess['fullname']), 0, 15).'...';
  }
?>

<header class="main-header header-border">
  <a href="<?php echo site_url('home') ?>" class="logo">
    <span class="logo-mini"><b>A</b>PP</span>
    <span class="logo-lg"><b>A</b>PP</span>
  </a>
  <nav class="navbar navbar-static-top">
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">        
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?php echo config_item('img'); ?>avatar5.png" class="user-image" alt="User Image">
            <span class="hidden-xs"><?php echo $fullname; ?></span>
          </a>
          <ul class="dropdown-menu">
            <li class="user-header">
              <img src="<?php echo config_item('img'); ?>avatar5.png" class="img-circle" alt="User Image">

              <p>
                <?php echo strtoupper($this->_sess['fullname']); ?><br><?php echo ucwords(strtolower($this->_sess['level_name'])); ?>
                <small>Terdaftar Sejak <?php echo $this->_sess['signup']; ?></small>
              </p>
            </li>
            <li class="user-footer">
              <div class="pull-left">
                <!-- <a href="#" class="btn btn-default btn-flat">Profil</a> -->
              </div>
              <div class="pull-right">
                <a href="javascript:;" class="btn btn-default btn-flat logout">Keluar</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
<script type="text/javascript">
  $('.logout').click(function(){
    blockUi();
    $.get('<?php echo site_url('logout') ?>',function( data ) {
      window.location.href = data.message;
      $.unblockUI();
    }, 'json')
  });
</script>
