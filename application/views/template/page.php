<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  if(!checkUrlAccess($this->uri->uri_string())){
    $this->load->view('template/403');
    return false;
  } 
?>

<!DOCTYPE html>
<html>
  <?php echo $this->load->view('template/assets'); ?>
  <body class="hold-transition skin-blue fixed sidebar-mini">
    <div class="wrapper">
      <?php echo $this->load->view('template/header'); ?>
      <?php echo $this->load->view('template/sidebar'); ?>
      <?php echo $this->load->view($content); ?>
      <?php //echo $this->load->view('template/footer'); ?>
      <div class="control-sidebar-bg"></div>
    </div>
  </body>
</html>
<script type="text/javascript">
  baseURL = '<?php echo site_url() ?>';
</script>
