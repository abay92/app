<?php
  $fullname = strtoupper($this->_sess['fullname']);
  if(strlen($fullname) > 15){
    $fullname = substr(strtoupper($this->_sess['fullname']), 0, 15).'...';
  }
?>

<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo config_item('img'); ?>avatar5.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $fullname; ?></p>
        <a><?php echo ucwords(strtolower($this->_sess['level_name'])); ?></a>
      </div>
    </div>
    <?php 
      echo listMenu($this->_sess['menu']); 
      $rpc = str_replace('/', '_', uri_string());
    ?> 
  </section>
</aside>
<script type="text/javascript">
  $(document).ready(function(){
    $('ul li a').each(function(e){
      uri = '<?php echo $rpc ?>';
      $('a#'+uri).parents("ul.treeview-menu").parent().addClass("active menu-open");
      $('a#'+uri).parent("li").addClass('active');
    });
  });
</script>