$.extend($.validator.messages, {
	required: "Kolom ini harus diisi.",
	remote: "Silahkan perbaiki data ini.",
	email: "Silahkan masukkan format email yang benar.",
	url: "Silakan masukkan format URL yang benar.",
	date: "Silakan masukkan format tanggal yang benar.",
	dateISO: "Silakan masukkan format tanggal(ISO) yang benar.",
	number: "Silakan masukkan angka yang benar.",
	digits: "Silahkan masukan angka saja.",
	creditcard: "Silahkan masukkan format kartu kredit yang benar.",
	equalTo: "Silahkan masukkan nilai yg sama dengan sebelumnya.",
	maxlength: $.validator.format("Input dibatasi hanya {0} karakter."),
	minlength: $.validator.format("Input tidak kurang dari {0} karakter."),
	rangelength: $.validator.format("Panjang karakter yg diizinkan antara {0} dan {1} karakter."),
	range: $.validator.format("Silahkan masukkan nilai antara {0} dan {1}."),
	max: $.validator.format("Silahkan masukkan nilai lebih kecil atau sama dengan {0}."),
	min: $.validator.format("Silahkan masukkan nilai lebih besar atau sama dengan {0}.")
});

function settingDefaultDatagrid(){
	$.extend($.fn.datagrid.defaults, {
		method      : 'POST',
		pageList    : [10,20,30,50,100,200],
		striped     : false,
		fitColumns  : true,
		rownumbers  : true,
		pagination  : true,
		height      : 'auto',
		toolbar     : '#tb',
		emptyMsg    : 'Tidak Ada Data.',
		loadMsg 	: 'Memproses, tunggu sebentar.',
		minHeight   : '200px',
		scrollbarSize: 0,
		nowrap      : true,
		sortable    : false,
		singleSelect: true,
	});
}

function getFormData($form){
	var unindexed_array = $form.serializeArray();
	var indexed_array = {};

	$.map(unindexed_array, function(n, i){
		indexed_array[n['name']] = n['value'];
	});

	return indexed_array;
}

function onLoadSuccessDatagrid(){
	$('#grid').datagrid('resize');

	$('.sidebar-toggle').click(function(){
		setTimeout(function(){
			$('#grid').datagrid('resize');
		},400)
	});

	$(window).resize(function(){
		setTimeout(function(){
			$('#grid').datagrid('resize');
		},400)
	})
}

function searchingDatagrid(){
	$('#grid').datagrid('load',{
		search  : $('#search').val(),  
	});
}

function showPassword() {
	var x = document.getElementById("password");
	if (x.type === "password") {
		x.type = "text";
	} else {
		x.type = "password";
	}
}

function showModal(url) {
	$.magnificPopup.open({
		items: { src: url },
		modal: true,
		type: 'ajax',
		tLoading: '<i class="fa fa-refresh fa-spin"></i> Mohon tunggu...',
		showCloseBtn: false,
    	focus: '.focus',
	});
}

function download(url,data) {
	var btn = $('#download');
	$.ajax({
		url         : url,
		data        : data,
		type        : 'POST',
		dataType    : 'json',

		beforeSend: function(){
			btn.button('loading');
		},

		success: function(json) {
			if(json.status == 'success'){
				d = json.data;

				var wb = XLSX.utils.book_new();
		        wb.Props = {
		            Title: d.title,
		            Subject: d.subject,
		            Author: d.author,
		            CreatedDate: new Date()
		        };
		        
		        wb.SheetNames.push(d.sheet_name);
		        var ws_data = d.download;
		        var ws = XLSX.utils.aoa_to_sheet(ws_data);
		        wb.Sheets[d.sheet_name] = ws;
		        var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});

		        function s2ab(s) {
					var buf = new ArrayBuffer(s.length);
					var view = new Uint8Array(buf);
					for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;
					return buf;
				}
		        saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), d.file_name+'.xlsx');
			}else{
				console.log(json.message);
			}
			
		},

		error: function() {
			console.log('Silahkan Hubungi Administrator');
		},

		complete: function(){
			btn.button('reset');
		}
	});
}

function closeModal() {
	$.magnificPopup.close();
}

function getFormData(form){
	var unindexed_array = form.serializeArray();
	var indexed_array = {};

	$.map(unindexed_array, function(n, i){
		indexed_array[n['name']] = n['value'];
	});

	return indexed_array;
}

rules	= {};
messages= {};

function validateForm(id,callback){
	$(id).validate({
		ignore      : 'input[type=hidden], .select2-search__field', 
		errorClass  : 'validation-error-label',
		successClass: 'validation-valid-label',
		rules   	: rules,
		messages	: messages,

		highlight   : function(element, errorClass) {
			$(element).removeClass(errorClass);
		},

		unhighlight : function(element, errorClass) {
			$(element).removeClass(errorClass);
		},

		errorPlacement: function(error, element) {
			if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
		        if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
		          error.appendTo( element.parent().parent().parent().parent() );
		        }
		        else {
		          error.appendTo( element.parent().parent().parent().parent().parent() );
		        }
		    }

		    // Unstyled checkboxes, radios
		    else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
		        error.appendTo( element.parent().parent().parent() );
		    }

		    // Input with icons and Select2
		    else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
		        error.appendTo( element.parent() );
		    }

		    // Inline checkboxes, radios
		    else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
		        error.appendTo( element.parent().parent() );
		    }

		    // Input group, styled file input
		    else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
		        error.appendTo( element.parent().parent() );
		    }

			else if (element.hasClass('in-group')) {
				error.appendTo($('.group').parent());
			}

			else {
				error.insertAfter(element);
			}
		},

		submitHandler: function(form) {
			if(typeof callback != 'undefined' && typeof callback == 'function') {
				callback(form.action,getFormData($(form)));
			}
		}
	});
}

function postLogin(url,data){
	var btn = $('#btnLogin');
	$.ajax({
		url         : url,
		data        : data,
		type        : 'POST',
		dataType    : 'json',

		beforeSend: function(){
			btn.button('loading');
		},

		success: function(json) {
			if(json.status == 'success'){
				window.location.href = json.message;
			}else{
				$('#fLogin')[0].reset();
				$('#username' ).focus();
				alertify.error(json.message);
				return false;
			}
		},

		error: function() {
			alertify.error('Silahkan Hubungi Administrator');
		},

		complete: function(){
			btn.button('reset');
		}
	});
}

function actionLogin(){
	validateForm('#fLogin', function(url,data){
		data.password = CryptoJS.MD5(data.password).toString()
		postLogin(url,data);
	});
}

function unBlockUiId(id){
	$('#'+id).block({
		message: '<h4><i class="fa fa-spinner fa-spin"></i> Loading</h4>',
	});
}

function blockUi(){
	$.blockUI({ 
		message: '<h4><i class="fa fa-spinner fa-spin"></i> Loading</h4>',
		// css: {
		// 	width:	'10%',
		// 	left:	'45%'
		// },
	});
}

function postData(url,data){
	$.ajax({
		url         : url,
		data        : data,
		type        : 'POST',
		dataType    : 'json',

		beforeSend: function(){
			unBlockUiId('ff')
		},

		success: function(json) {
			if(json.status == 'success'){
				closeModal();
				alertify.success(json.message);
				$('#grid').datagrid('reload');
			}else{
				alertify.error(json.message);
			}
		},

		error: function() {
			alertify.error('Silahkan Hubungi Administrator');
		},

		complete: function(){
			$('#ff').unblock(); 
		}
	});
}

function confirmationAction(message,url){
	alertify.confirm(message, function (e) {
		if(e){
			returnConfirmation(url)
		}
	});
}

function returnConfirmation(url){
	$.ajax({
		url         : url,
		type        : 'GET',
		dataType    : 'json',

		beforeSend: function(){
			blockUi()
		},

		success: function(json) {
			if(json.status == 'success'){
				alertify.success(json.message);
				$('#grid').datagrid('reload');
			}else{
				alertify.error(json.message);
			}
		},

		error: function() {
			alertify.error('Silahkan Hubungi Administrator');
		},

		complete: function(){
			$.unblockUI();
		}
	});
}

function gridPrivilege(id){
	$('#grid').treegrid({
		url         : baseURL+'privilege/get_list',
		method      : 'POST',
		queryParams : {
			group : id,
		},
		striped     : false,
		fitColumns  : true,
		treeField   : 'name',
		idField     : 'menu_id',
		height      : 'auto',
		scrollbarSize: 0,
		toolbar     : '#tb',
		nowrap      : true,
		sortable    : false,
		singleSelect: true,
		emptyMsg    : 'Tidak Ada Data.',
		loadMsg 	: 'Memproses, tunggu sebentar.',
		columns:[[
			{ field: 'name', title: 'Name', width: 30},
			{ field: 'action', title: 'Action', width: 100},
		]],

		onLoadSuccess: function(row, data){
			// $('.tree-folder').css('background','none');
			// $('.tree-file').css('background','none');

			$('.sidebar-toggle').click(function(){
				setTimeout(function(){
					$('#grid').treegrid('resize');
				},400)
			});

			$(window).resize(function(){
				setTimeout(function(){
					$('#grid').treegrid('resize');
				},400)
			})

			// r = data.rows;
			// for(i in r){
			// 	$('.'+r[i].iconCls).html('<i class="fa fa-'+r[i].iconCls+'"></i>');
			// }

			// $('.circle-o').html('<i class="fa fa-circle-o"></i>');
			$('.act').iCheck({
				checkboxClass: 'icheckbox_flat-blue',
				radioClass   : 'iradio_flat-blue'
			})

			.on('ifClicked', function (e) {
				if(e.currentTarget.checked){
					status = 0;
				}else{
					status = 1;
				}
				d      = e.currentTarget.dataset;
				group  = $('#group').select2('val');
				post = {
					menu_id: d.menu_id,
					detail_id: d.detail_id,
					privilege: d.privilege,
					group: group,
					status: status,
				}

				arr  = [];
				arr.push(post)

				send = {params : arr}
				savePrivilege(send)
			})

			$('#grid').treegrid('resize');
		}
	});
}

function savePrivilege(data){
	$.ajax({
		url         : baseURL+'privilege/set_privilege',
		data        : data,
		type        : 'POST',
		dataType    : 'json',

		beforeSend: function(){
			blockUi()
		},

		success: function(json) {
			if(json.status == 'success'){
				alertify.success(json.message);
				$('#grid').treegrid('reload');
			}else{
				alertify.error(json.message);
			}
		},

		error: function() {
			alertify.error('Silahkan Hubungi Administrator');
		},

		complete: function(){
			$.unblockUI();
		}
	});
}

function validatorPassword(){
	$.validator.addMethod('password', function (value) { 
		return /(?=^.{7,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(value); 
	}, 'Minimal 7 karakter, 1 angka, 1 huruf kecil dan 1 huruf kapital');
}

function formatRupiah(angka, prefix){
	var number_string = angka.replace(/[^,\d]/g, '').toString(),
	split    = number_string.split(','),
	sisa     = split[0].length % 3,
	rupiah   = split[0].substr(0, sisa),
	ribuan   = split[0].substr(sisa).match(/\d{3}/gi);

	if (ribuan) {
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}

	rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function removeRupiah(str){
	return str.split('.').join('');
}

function iconFormat(icon) {
	return "<i class='fa fa-" + icon.text + "'></i> " + icon.text;
}

function isNumberKey(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	return true;
}